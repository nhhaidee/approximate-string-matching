#ifndef UTIL_H
#define UTIL_H
#include<iostream>
#include<string>
#include<vector>
#include"DataTypes.h"

#define EOC '%'
#define BIT_SET(a,b) ((a) |= (1<<(b)))
#define BIT_CLEAR(a,b) ((a) &= ~(1<<(b)))
#define BIT_FLIP(a,b) ((a) ^= (1<<(b)))
#define BIT_CHECK(a,b) ((a) & (1<<(b)))



using namespace std;

int gettimeofday(struct  timeval*tv,struct  timezone *tz );

void DNA_Translate(string *orig_str)
{
    for(string::iterator it=orig_str->begin();it<orig_str->end();it++)
    { 
        
        if(*it=='N' || *it=='n')
        {
            *it='N'; // Not use N, so change to character N to Z after T
        }     
        if(*it=='a')
        {
            *it='A';
        }
        if(*it=='c')
        {
            *it='C';
        }
        if(*it=='g')
        {
            *it='G';
        }
        if(*it=='t')
        {
            *it='T';
        }
        if(*it=='W' || *it=='w')
        {
            *it='T';
        }
        if(*it=='S' || *it=='s')
        {
            *it='G';
        }
        if(*it=='M' || *it=='m')
        {
            *it='C';
        }
        if(*it=='K' || *it=='k')
        {
            *it='T';
        }
        if(*it=='R' || *it=='r')
        {
            *it='G';
        }
        if(*it=='Y' || *it=='y')
        {
            *it='C';
        }
        if(*it=='B' || *it=='b')
        {
            *it='G';
        }
        if(*it=='D' || *it=='d')
        {
            *it='T';
        }
        if(*it=='H' || *it=='h')
        {
            *it='C';
        }
        if(*it=='V' || *it=='v')
        {
            *it='G';
        }
    }
}

u_int8_t encode(char &c)
{
    u_int8_t i;
    switch(c)
    {
        case 'a':
        case 'A': i=0;break;
        case 'c':
        case 'C': i=1;break;
        case 'g':
        case 'G': i=2;break;
        case 't':
        case 'T': i=4;break;
        default: i=3;break;
    }
    return i;
}

char decode(size_t i)
{
    char c;
    switch(i)
    {
        case 0: c='A';break;
        case 1: c='C';break;
        case 2: c='G';break;
        case 4: c='T';break;
        default: c='N';break;
    }
    return c;
}

class FastaHead
{
public:
    string name;
    clint_t length;
    clint_t start; // if fasta is reversed then (start,end],
    clint_t end;   // else, [start,end)

    FastaHead(string s,clint_t l)
    {
        name=s;
        length=l;
        start=-1;
        end=-1;
    }

};

class RealPosition
{
public:
    string name;
    clint_t pos;
};

class ChromIndex
{
public:
    ChromIndex()
    {
        READY=false;
    }

    void addhead(FastaHead head)
    {
        heads.push_back(head);
    }

    void adjust()
    {
        clint_t tmp_start=0;
        for(size_t i=heads.size()-1;i>0;i--)
        {
            heads[i].start=tmp_start;
            heads[i].end=tmp_start+heads[i].length;
            tmp_start=heads[i].end+1;
        }
        heads[0].start=tmp_start;
        heads[0].end=tmp_start+heads[0].length;
        READY=true;
    }

    RealPosition calRealPos(clint_t &p,size_t l)
    {
        if(!READY)
            adjust();
        for(size_t i=0;i<heads.size();i++)
        {
            if(p>heads[i].start)
            {
                if(p<=heads[i].end)
                {
                    rpos.pos=heads[i].length-p+heads[i].start-l+2;
                    rpos.name=heads[i].name;
                }
                else
                {
                    cout<<"read wrong, because it matches to 'end marker'"<<endl;
                    rpos.pos=p;
                    rpos.name="WrongRead";
                }
                break;
            }
        }
        return rpos;
    }

private:


    vector<FastaHead> heads;
    RealPosition rpos;
    bool READY;

};





#endif // UTIL_H
