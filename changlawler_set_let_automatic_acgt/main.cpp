
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sys/time.h>
#include <string>
#include <zlib.h>
#include "kseq.h"
#include "SuffixTree.h"
#include "DataTypes.h"

KSEQ_INIT(gzFile, gzread)

using namespace std;

int main()
{

    struct timeval start,start1,start2,finish,finish1,finish2;
    int l;
    double duration, duration1,duration2;

     
    string fasta_file="Cyanidioschyzon_merolae.ASM9120v1.dna.chromosome.1.fa";

    string fastq_file="Cyanidioschyzon_merolae_reads_100bps.fq";
    string out_path="./out.txt";
    string fasta_seq;
    
    gzFile fp_fa;
    kseq_t *seq;
    fp_fa = gzopen(fasta_file.c_str(), "r");
    seq = kseq_init(fp_fa);   
    while ((l = kseq_read(seq)) >= 0)
    {
        //FastaHead head(seq->name.s,seq->seq.l);
        //chrindex.addhead(head);
        fasta_seq.append(seq->seq.s);
        fasta_seq.push_back('%');
    }
    //fasta_seq_orig = fasta_seq;
    DNA_Translate(&fasta_seq);
    //cout << fasta_seq[fasta_seq.length()] << endl;
    cout << "Genome Length: " << fasta_seq.length() << endl;
    kseq_destroy(seq);
    gzclose(fp_fa);

   //Test with fastqc 
    //vector <double>running_time_LET;
    array<double,10>running_time_LET;
    array<double,10>running_time_SET;
    running_time_LET.fill(0);
    running_time_SET.fill(0);

    int k;
    int count = 0;
    gzFile fp_fq;
    kseq_t *read;
    string pattern;
    fp_fq = gzopen(fastq_file.c_str(), "r");
    read = kseq_init(fp_fq);

    clint_t fasta_seq_len = fasta_seq.length()-1;
    int read_len;            
    while ((l = kseq_read(read)) >= 0)
    {
        count++;
        
        //cout << "Read Sequence: " << read->seq.s << endl;
        SuffixTree stree;
        cout << "Read Sequence Length: " << read->seq.l << ", Read No: " << count << endl;
        string q(read->seq.s);   
        pattern = q;
        pattern.push_back('$'); 
        cout << "Pattern: " << pattern << endl;
        read_len = pattern.length()-1;
        
        ///////////// Build Suffix Tree and Matching Statistics////////////////////
        gettimeofday(&start,NULL);
        stree.buildSuffixTree(pattern);
        cout << "Done Suffix Tree " << endl;
        stree.matchingStatistics(pattern, fasta_seq);
        cout << "Done Matching statistics " << endl;
        gettimeofday(&finish,NULL);
        duration=finish.tv_sec-start.tv_sec+(finish.tv_usec-start.tv_usec)/1000000.0;
        cout << "MS: " << duration << endl;
        //////////////////////////////////////////////////////////////////////////////
        int y = 0;
        for (int err =1; err <= 7; err = err + 1)
        {
        ////////////////////////CL.LET///////////////////////////////////////////////
            
            k = err;
            cout << "k = " << err << endl;
            y++;
            cout << "Starting CL.LET" << endl;
            gettimeofday(&start1,NULL);
            stree.changlawler_let(fasta_seq_len, read_len, k);
            gettimeofday(&finish1,NULL);
            duration1=finish1.tv_sec-start1.tv_sec+(finish1.tv_usec-start1.tv_usec)/1000000.0;
            /////////////////////////////////////////////////////////////////////////////

            ////////////////////////CL.SET///////////////////////////////////////////////
            if (err <=10)
            {
                cout << "Starting CL.SET" << endl;
                gettimeofday(&start2,NULL);
                stree.changlawler_set(fasta_seq_len, read_len, k);
                gettimeofday(&finish2,NULL);
                duration2=finish2.tv_sec-start2.tv_sec+(finish2.tv_usec-start2.tv_usec)/1000000.0;
                running_time_SET[y-1] = running_time_SET[y-1] + (duration+duration2);
            }
            running_time_LET[y-1] = running_time_LET[y-1] + (duration+duration1);
            cout << "k = " << err << endl;
             
        }
        stree.free_memory_usage();
        if (count ==  100)
            break;

    }
    cout<<"read amount: "<<count<<endl;
    kseq_destroy(read);
    gzclose(fp_fq);

    cout << "CL.LET Result" << endl;
    for (int i = 0; i< running_time_LET.size();i++)
    {
        cout << "k = " << i + 1 << ", running time = " << running_time_LET[i] << endl;
    }
    cout << "CL.SET Result " << endl;
    for (int i = 0; i< running_time_SET.size();i++)
    {
        cout << "k = " << i + 1 << ", running time = " << running_time_SET[i] << endl;
    }

    return 0;

}
