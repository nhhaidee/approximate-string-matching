#ifndef SuffixTree_H
#define SuffixTree_H

#include<iostream>
#include<string>
#include<vector>
#include <array>
#include <unordered_map>
#include <algorithm>
#include "math.h"
#include "DataTypes.h"


using namespace std;
class SuffixTree
{   
    private:

        struct Intervals
        {
            clint_t substr_start;
            clint_t substr_end;
        };

        struct SuffixTreeNode 
        {
            SuffixTreeNode *children[MAX_CHILD];
            SuffixTreeNode *suffixLink;
            int start;
            int *end;
            int substr_len; // Label length of substring (from node to root)
            //int level; //Level of each node in suffix tree
            int id; // unique id for each node in suffix tree which using for euler tour
            int suffixIndex; // Internal node has suffix index is -1
        };
        typedef SuffixTreeNode Node;   

        Node *newNode(int start, int *end)
        {
            Node * node = new Node;
            int i;
            for (i = 0; i < MAX_CHILD; i++)
                node->children[i] = NULL;
            node->suffixLink = root;
            node->start = start;
            node->end = end;
            node->suffixIndex = -1;
            node->substr_len = 0;
            //node->level = 0;
            node->id = 0;
            return node;
        }

        Node* root;
        Node* lastNewNode;
        Node* activeNode;
        int remainingSuffixCount;
        int leafEnd;
        int activeEdge;
        int activeLength;
        int *rootEnd;
        int *splitEnd;
        //variable utils
        int total_nodes; // Total of nodes in suffix tree
        //array<Node*, 2*total_nodes -1>euler;
        vector<Node*> euler;
        vector<int> node_level;
        array<int, 400> first_occ; // First of occurrence of node during euler tour
        int ind;
        //unordered_map<int, Node*> suffixTreeNode; // Hash table to store pair of (nodeid----> node add) just for testing
        unordered_map<int, Node*> leafNode;
        //Using for matching statistic
        vector<int> M;
        vector<Node*> M1;
        int **lookup_table;

    public:

        SuffixTree();
        ~SuffixTree();
        //////////////////////////Functions for suffix tree of pattern////////////////////////////
        int edgeLength(Node *n);
        int walkDown(Node *currNode);
        void extendSuffixTree(int pos, string pattern);
        void setSuffixIndexByDFS(Node *n, int labelHeight, string T);
        void setEdgeLength();
        void freeSuffixTree(Node *n);
        void buildSuffixTree(string pattern);
        /////////////////////////////////////////////////////////////////////////////////////////


        /////////////////////////// Function for matching statistic//////////////////////////////
        void matchingStatistics(string Pattern, string T);
        int len(Node* r, char c); 
        int first(Node* r, char c);
        Node *child(Node* r, char c) // Find children of a node which using for matching statistic
        {
            if (r->children[encode(c)] != NULL)
                return r->children[encode(c)];
            else 
                return NULL;  
        }
        ////////////////////////////////////////////////////////////////////////////////////////

        ////////////////////////// Functions for finding LCA of suffix Tree////////////////////
        void eulerTour(Node *n, int l);
        void RMQpreprocess(vector<int>node_level)
        {
            int col;
            int size = node_level.size();
            col = log2(size) + 1;

            lookup_table = new int*[size];
            for (int i = 0; i < size ; i++)
                lookup_table[i] = new int[col];
            
            for (int i = 0; i < size; i++)
                lookup_table[i][0] = i;
 
            for (int j=1; (1<<j)<=size; j++)
            {
            // Compute minimum value for all intervals with size 2^j
                for (int i=0; (i+(1<<j)-1) < size; i++)
                { 
                    // For arr[2][10], we compare arr[lookup[0][3]] and
                    // arr[lookup[3][3]]
                    if (node_level[lookup_table[i][j-1]] < node_level[lookup_table[i + (1<<(j-1))][j-1]])
                        lookup_table[i][j] = lookup_table[i][j-1];
                    else
                        lookup_table[i][j] = lookup_table[i + (1 << (j-1))][j-1];      
                }
            }
        }
        int RMQlookup(int qs, int qe)
        {
            
            int j = (int)log2(qe-qs+1);
            if (node_level[lookup_table[qs][j]] <= node_level[lookup_table[qe - (1<<j) + 1][j]])
                return lookup_table[qs][j];
            else return lookup_table[qe - (1<<j) + 1][j];

        }
        int LCA(Node* u, Node* v) // Return LCA node of node u and v
        {
             int qs;
             int qe;
             if (u == v)
                return u->substr_len;
             if (first_occ[u->id] > first_occ[v->id])
             {
                 qs = first_occ[v->id];
                 qe = first_occ[u->id];
             }
             else
             {
                 qs = first_occ[u->id];
                 qe = first_occ[v->id];
             }
             //cout << "u->id " << u->id << ", v->id " << v->id << endl; 
             //Normal way to do, reduce LCA for RMQ 
             /*
             int min =node_level[qs];;
             int index = qs;
             for (int i = qs; i <= qe; i++)
             {
                if (min >= node_level[i])
                {
                    min = node_level[i];
                    index = i;
                }
             }
             */
             return euler[RMQlookup(qs,qe)]->substr_len;
        }
        /////////////////////////////////////////////////////////////////////////////////////////////
        // Util functions
        u_int8_t encode(char &c)
        {
            u_int8_t i;
            switch(c)
            {
                case 'a':
                case 'A': i=0;break;
                case 'c':
                case 'C': i=1;break;
                case 'g':
                case 'G': i=2;break;
                case 't':
                case 'T': i=3;break;
                default:  i=4;break; // $
            }
            return (u_int8_t)i;
        }
        int jump(clint_t i, clint_t j);//J(i, j) = min{M[j], LCA(M'[j], suffix x(i,m)&)),j is index of matching statistics array, i is the suffix index of pattern
        void MatrixL(int read_len, int k, clint_t substring_len);
        void MatrixL_let(int read_len, int k, clint_t start_pos, clint_t end_pos);
        void mergeIntervals(vector<Intervals> sub_interval, int read_len, int k);
        void changlawler_let(int read_len, int k, clint_t fasta_seq_len);
        int transformIndex(clint_t x)
        {
            return x - 2;
        }
        int maximum(clint_t i, clint_t j, clint_t k)
        {
            return max(max(i,j),k);
        }    
};
#endif
