#include "utils.h"
#include "DataTypes.h"

void DNA_Translate(string *orig_str)
{
    for(string::iterator it=orig_str->begin();it<orig_str->end();it++)
    { 
        
        if(*it=='N' || *it=='n')
        {
            *it='Z'; // Not use N, so change to character N to Z after T
        }     
        if(*it=='a')
        {
            *it='A';
        }
        if(*it=='c')
        {
            *it='C';
        }
        if(*it=='g')
        {
            *it='G';
        }
        if(*it=='t')
        {
            *it='T';
        }
        if(*it=='W' || *it=='w')
        {
            *it='T';
        }
        if(*it=='S' || *it=='s')
        {
            *it='G';
        }
        if(*it=='M' || *it=='m')
        {
            *it='C';
        }
        if(*it=='K' || *it=='k')
        {
            *it='T';
        }
        if(*it=='R' || *it=='r')
        {
            *it='G';
        }
        if(*it=='Y' || *it=='y')
        {
            *it='C';
        }
        if(*it=='B' || *it=='b')
        {
            *it='G';
        }
        if(*it=='D' || *it=='d')
        {
            *it='T';
        }
        if(*it=='H' || *it=='h')
        {
            *it='C';
        }
        if(*it=='V' || *it=='v')
        {
            *it='G';
        }
    }
}


u_int8_t encode(char &c)
{
    u_int8_t i;
    switch(c)
    {
        case 'A': i=0;break;
        case 'C': i=1;break;
        case 'D': i=2;break;
        case 'E': i=3;break;
        case 'F': i=4;break;
        case 'G': i=5;break;
        case 'H': i=6;break;
        case 'I': i=7;break;
        case 'K': i=8;break;
        case 'L': i=9;break;
        case 'M': i=10;break;
        case 'N': i=11;break;
        case 'P': i=12;break;
        case 'Q': i=13;break;
        case 'R': i=14;break;
        case 'S': i=15;break;
        case 'T': i=16;break;
        case 'V': i=17;break;
        case 'W': i=18;break;
        case 'Y': i=19;break;
        case 'Z': i=20;break; // CHILDREN_SIZE = 4
        default: i=CHILDREN_SIZE+2;break; // i = 6 which is %, CHILDREN_SIZE + 2 = 6
    }

    if(i==CHILDREN_SIZE+2)
    {
        if(c=='$')
            i=CHILDREN_SIZE+1; // i = 4 + 1 = 5,CHILDREN_SIZE + 1 = 5
    }
    return (u_int8_t)i;
}

char decode(size_t i)
{
    char c;
    switch(i)
    {
        case 0: c='A';break;
        case 1: c='C';break;
        case 2: c='D';break;
        case 3: c='E';break;
        case 4: c='F';break;
        case 5: c='G';break;
        case 6: c='H';break;
        case 7: c='I';break;
        case 8: c='K';break;
        case 9: c='L';break;
        case 10: c='M';break;
        case 11: c='N';break;
        case 12: c='P';break;
        case 13: c='Q';break;
        case 14: c='R';break;
        case 15: c='S';break;
        case 16: c='T';break;
        case 17: c='V';break;
        case 18: c='W';break;
        case 19: c='Y';break;
        case 20: c='Z';break;
        case 21: c='$';break;
        default: c='%';break;
    }
    return c;
}
