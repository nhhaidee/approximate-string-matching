///Author:Hai Nguyen
///Created on Oct 2018


#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sys/time.h>
#include <vector>
#include <array>
#include <string>
#include <unordered_map>
#include <algorithm>
/*
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
*/
#include <zlib.h>
#include "kseq.h"
#include "bwt.h"
#include "utils.h"
#include "EDTree.h"
#include "DataTypes.h"


KSEQ_INIT(gzFile, gzread)

int qgrams_length = 3;

struct Text_intervals
{
    int64_t start_point;
    int64_t end_point;
};
bool cmp (pair<string, vector<int>> &a, pair<string, vector<int>> &b)
{
    return a.first < b.first;
}

void sort(unordered_map<string, vector<int>> & qgrams_map, vector<pair<string, vector<int>>> &sorted_qgrams)
{
    for (auto& it: qgrams_map){
        pair<string, vector<int>> temp;
        temp.first  = it.first;
        temp.second = it.second;
        sorted_qgrams.push_back(it);
    }
    sort(sorted_qgrams.begin(), sorted_qgrams.end(), cmp);
}

bool compareInterval(Text_intervals l1, Text_intervals l2)
{
    return (l1.start_point < l2.start_point);
}

void dp (string P, string& T, fmint_t k, fmint_t start, fmint_t end){
	vector<int> H;
	H.clear();
	H.shrink_to_fit();
	int top = k + 1;
	int C, E;
    fmint_t pos;
    fmint_t j;
	for (int i = 0; i <= P.length(); i++) {
		H.push_back(i);
	}
	for (j = start; j <= end; j++) { // For each column
		C = 0;
		if (T[j] == '%')
		{
			H.clear();
			H.shrink_to_fit();
			top = k + 1;
			for (int i = 0; i <= P.length(); i++) 
			{
				H.push_back(i);
			}
		}
		for (int i = 1; i <= top; i++) { // Scan from 1 to k
			if (P[i-1] == T[j])
				E = C;
			else
				E = min(min(H[i - 1] + 1, H[i] + 1), C + 1);
			C = H[i];
			H[i] = E;
		}
		while (H[top] > k)
			top = top - 1;
		if (top == P.length())
        
			cout << "Position: " << j << " k: " << H[P.length()] << endl;
            //pos = j;
		else
			top = top + 1;
	}

}


int BinarySearchQGrams (vector<pair<string, vector<int>>> &qgrams_sorted, string qgrams, int l, int r)
{ 
   if (r >= l)
   {
       int mid = l + (r-l) / 2;
       if (qgrams_sorted[mid].first == qgrams)
       {
           return mid;
       }
       if (qgrams_sorted[mid].first > qgrams)
       {
           return BinarySearchQGrams(qgrams_sorted, qgrams, l, mid - 1);
       }
       return BinarySearchQGrams(qgrams_sorted, qgrams, mid + 1, r);
   }
   return -1;
}

bool Prefix (string a, string b) // check is a is a prefix of b
{
    if (b.substr(0, a.length())== a)
        return true;
    else 
        return false;
}

int NumberOfVerification(vector<pair<string, vector<int>>> &qgrams_sorted, string& qrams)
{
    if (qrams.length() >= qgrams_length)
    {
        string sub_qgrams = qrams.substr(0, qgrams_length); // truncated to its first q letter
        int pos = BinarySearchQGrams(qgrams_sorted, sub_qgrams, 0, qgrams_sorted.size()-1);
        if (pos == -1)
            return -1;
        else
            return qgrams_sorted[pos].second.size();
    }
    else
    {
        int total_verfifications = 0;
        int endpoint = -1;
        for (int i  = 0; i < qgrams_sorted.size(); i++)
        {
            if (Prefix(qrams, qgrams_sorted[i].first))
            {
                endpoint = i;
                total_verfifications = total_verfifications + qgrams_sorted[i].second.size();
            }     
        }
        total_verfifications = total_verfifications - qgrams_sorted[endpoint].second.size();
        if (total_verfifications <= 0)
            return -1;
        else
            return total_verfifications;
    }
}

int NumberOfVerificationV2(unordered_map<string, vector<int>> qgrams_map, string& qrams)
{
    if (qrams.length() >= qgrams_length)
    {
        string sub_qgrams = qrams.substr(0, qgrams_length); // truncated to its first q letter
        return qgrams_map[sub_qgrams].size();
    }
    else
    {
        int total_verfifications = 0;
        string endpoint = "";
        for (auto it  = qgrams_map.begin(); it != qgrams_map.end(); it++)
        {
            if (Prefix(qrams, it->first))
            {
                endpoint = it->first;
                total_verfifications = total_verfifications + qgrams_map[it->first].size();
            }     
        }
        total_verfifications = total_verfifications - qgrams_map[endpoint].size();
        if (total_verfifications <= 0)
            return -1;
        else
            return total_verfifications;
    }
}

using namespace std;


//int main(int argc,char *argv[])
int main()
{
    struct timeval start,finish;
    struct timeval start_phase1,finish_phase1;
    struct timeval start_phase2,finish_phase2;
    array<double,10>total_phase1;
    array<double,10>total_phase2;
    int l, count=0;
    double duration;
    vector<double> runtime;
    vector<int> totalpoints;

    string fasta_file="proteinsequence30M.fa";


    string fastq_file="proteinreads_1read.fq";
    string out_path="./out.txt";
    cout <<" Species " << fasta_file << endl;
    cout <<" Species (Fastq) " << fastq_file << endl;


    fmint_t fasta_length;
    ChromIndex chrindex;
    string fasta;
    string fasta_seq, fasta_seq_orig;
    FMIndex fmindex;
    {
        gzFile fp_fa;
        kseq_t *seq;
        fp_fa = gzopen(fasta_file.c_str(), "r");
        seq = kseq_init(fp_fa);   
        while ((l = kseq_read(seq)) >= 0)
        {
            FastaHead head(seq->name.s,seq->seq.l);
            chrindex.addhead(head);
            fasta_seq.append(seq->seq.s);
            //fasta_seq.push_back('%');
        }
        kseq_destroy(seq);
        gzclose(fp_fa);
        chrindex.adjust();
        fasta_length = fasta_seq.length();

        fasta_seq_orig =fasta_seq;

        gettimeofday(&start,NULL);

    }

    cout << "Indexing qrams..." << endl;
    std::unordered_map<string, vector<int>> qgrams_map;
    for (int i = 0; i < (fasta_length - qgrams_length + 1); i++)
    {
        string qgrams = fasta_seq_orig.substr(i, qgrams_length);
        qgrams_map[qgrams].push_back(i);
    }

    vector<pair<string, vector<int>>> qgrams_sorted;
    sort(qgrams_map, qgrams_sorted);
    qgrams_map.clear();
    cout << "Done Index qrams..." << endl;

    int k;

    int total_ver_points;
    int y = 0;
    total_phase1.fill(0);
    total_phase2.fill(0); 
    for(int err = 1; err <= 7; err= err+1)
    {
        gzFile fp_fq;
        kseq_t *read;
        fp_fq = gzopen(fastq_file.c_str(), "r");
        read = kseq_init(fp_fq);
        k = err;
        total_ver_points = 0;
        gettimeofday(&start,NULL);
        count = 0;
        int m;
        y++;
        while ((l = kseq_read(read)) >= 0)
        {
            count++;
            //cout << "Read Sequence: " << read->seq.s << endl;
            cout << "Read Sequence Length: " << read->seq.l << ", Read #" << count << endl;
            string q(read->seq.s); 
            m = q.length();
            int **P = new int*[m];

            for (int i = 0; i < m; i++)
                P[i] = new int[k+1];
            
            for (int i = 0; i < m; i++)
                for (int j = 0; j < k + 1; j++)
                    P[i][j] = -112;
            

            int **C = new int*[m];
            for (int i = 0; i < m; i++)
                C[i] = new int[k];

            int **R = new int*[m];
            for (int i = 0; i < m; i++)
                R[i] = new int[m];

            for (int i = 0; i < m; i++)
                for (int j = 0; j < m; j++)
                    R[i][j] = -1;

            
            gettimeofday(&start_phase1,NULL);
            for (int i = 0; i < m; i++)
            {
                string sub = q.substr(i, m - i);
                R[i][m] = NumberOfVerification(qgrams_sorted, sub);
                P[i][0] = R[i][m];
                C[i][0] = m;
            }

            for (int r = 1; r <= k; r++)
            {
                for (int i = 0; i < (m - r); i++)
                {
                    if (R[i][i+1] == -1)
                    {
                        string FirstsubR = q.substr(i, 1);

                        R[i][i+1] = NumberOfVerification(qgrams_sorted, FirstsubR);
                    }
                    int min = R[i][i+1] + P[i + 1][r -1];
                    for (int j = (i + 1); j <= (m - r); j++)
                    {
                        if (R[i][j] == -1)
                        {
                            string subR = q.substr(i, j - i);
                            R[i][j] = NumberOfVerification(qgrams_sorted, subR);
                        }
                        if (min >= (R[i][j] + P[j][r -1]))
                            min = R[i][j] + P[j][r -1];
                    }
                    P[i][r] = min;
                }
            }
            gettimeofday(&finish_phase1,NULL);
            double duration1=finish_phase1.tv_sec-start_phase1.tv_sec+(finish_phase1.tv_usec-start_phase1.tv_usec)/1000000.0;
            total_phase1[y-1] = total_phase1[y-1] + duration1;
            total_ver_points = total_ver_points + P[0][k];
            cout << "P[0][k]: " << P[0][k] <<endl;
            //////////////// Verification phase ///////////////
            gettimeofday(&start_phase2,NULL);
            if ((P[0][k]*(m+2*k)) > fasta_length)
                dp (q, fasta_seq_orig, k, 0, fasta_length -1);
            else
                dp (q, fasta_seq_orig, k, 0, (P[0][k]*(m+2*k)));
            gettimeofday(&finish_phase2,NULL);
            double duration2=finish_phase2.tv_sec-start_phase2.tv_sec+(finish_phase2.tv_usec-start_phase2.tv_usec)/1000000.0;
            total_phase2[y-1] = total_phase2[y-1] + duration2;
            ///////////////////////////////////////////////////

            for (int i =0; i < m; i++)
            {
                delete [] R[i];
            }  
            delete [] R;
            for (int i =0; i < k + 1; i++)
                delete [] P[i];
            delete [] P;
            if (count == 1)
                break;
        }
        gettimeofday(&finish,NULL);
        duration=finish.tv_sec-start.tv_sec+(finish.tv_usec-start.tv_usec)/1000000.0;
        kseq_destroy(read);
        gzclose(fp_fq);
        cout << "k = " << err;
        cout<<", qgrams splitting time: "<< duration <<" s"; 
        runtime.push_back(duration);
        cout << ", total_ver: " << total_ver_points/100 << endl;
        totalpoints.push_back(total_ver_points/100);
    }
    for (int i = 0; i < runtime.size(); i++)
    {
        cout << "time: " << runtime[i] << ", phase1 : " << total_phase1[i] << ", phase2:" << total_phase2[i] << ", avg checkpoints: " << totalpoints[i]<< endl;
    }
    return 0;
}
