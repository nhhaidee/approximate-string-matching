#ifndef _DATA_TYPES_H
#define _DATA_TYPES_H

#include <stdint.h>


#define CHILDREN_SIZE 4
#define MAX_CHILD 5

#define FREE_NODE(p) \
    delete p; \
    p = NULL;

#define FREE_COL(col) \
    col.clear(); \
    col.shrink_to_fit();

#define MAXREADLEN 200
	
typedef uint16_t edint;
typedef int64_t saint64_t;
typedef uint32_t fmint_t;
typedef int64_t clint_t;
typedef unsigned int cluint_t;

    /*
	typedef unsigned char UINT8;
	typedef signed short INT16;
	typedef unsigned short UINT16;
	typedef unsigned int UINT32;
    typedef int edint;
	typedef int INT32;
	typedef char CHAR;
	typedef short SHORT;
	typedef long LONG;
	typedef int INT;
	typedef unsigned int UINT;
	typedef unsigned long DWORD;
	typedef unsigned char BYTE;
	typedef unsigned short WORD;
	typedef float FLOAT;
	typedef double DOUBLE;
	typedef int BOOL;*/

#endif