///Author:Hai Nguyen
///Created on Oct 2018


#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sys/time.h>
#include <vector>
#include <array>
#include <string>
#include <tr1/unordered_map>
#include <algorithm>
/*
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
*/
#include <zlib.h>
#include "kseq.h"
#include "bwt.h"
#include "utils.h"
#include "EDTree.h"
#include "DataTypes.h"


KSEQ_INIT(gzFile, gzread)

struct Text_intervals
{
    int64_t start_point;
    int64_t end_point;
};

bool compareInterval(Text_intervals l1, Text_intervals l2)
{
    return (l1.start_point < l2.start_point);
}

void dp (string P, string& T, fmint_t k, fmint_t start, fmint_t end){
	vector<int> H;
	H.clear();
	H.shrink_to_fit();
	int top = k + 1;
	int C, E;
    fmint_t pos;
    fmint_t j;
	for (int i = 0; i <= P.length(); i++) {
		H.push_back(i);
	}
	for (j = start; j <= end; j++) { // For each column
		C = 0;
		if (T[j] == '%')
		{
			H.clear();
			H.shrink_to_fit();
			top = k + 1;
			for (int i = 0; i <= P.length(); i++) 
			{
				H.push_back(i);
			}
		}
		for (int i = 1; i <= top; i++) { // Scan from 1 to k
			if (P[i-1] == T[j])
				E = C;
			else
				E = min(min(H[i - 1] + 1, H[i] + 1), C + 1);
			C = H[i];
			H[i] = E;
		}
		while (H[top] > k)
			top = top - 1;
		if (top == P.length())
			cout << "Position: " << j << " k: " << H[P.length()] << endl;
            //pos = j;
		else
			top = top + 1;
	}

}

using namespace std;

//int main(int argc,char *argv[])
int main()
{
    struct timeval start,finish;
    struct timeval start_phase1,finish_phase1;
    struct timeval start_phase2,finish_phase2;
    int l, count=0;
    double duration;

    string fasta_file="proteinsequence30M.fa";


    string fastq_file="proteinreads_1read.fq";
    //string fastq_file="/home/xielab101/Documents/programming/Source_Code/reads/5000000simulated_reads_on_Danio_rerio.GRCz11.fq";
    string out_path="./out.txt";
    cout <<" Species " << fasta_file << endl;
    cout <<" Species (Fastq) " << fastq_file << endl;


    fmint_t fasta_length;
    ChromIndex chrindex;
    string fasta;
    string fasta_seq, fasta_seq_orig;
    FMIndex fmindex;
    {
        gzFile fp_fa;
        kseq_t *seq;
        fp_fa = gzopen(fasta_file.c_str(), "r");
        seq = kseq_init(fp_fa);   
        while ((l = kseq_read(seq)) >= 0)
        {
            FastaHead head(seq->name.s,seq->seq.l);
            chrindex.addhead(head);
            fasta_seq.append(seq->seq.s);
            fasta_seq.push_back('%');
        }
        kseq_destroy(seq);
        gzclose(fp_fa);
        chrindex.adjust();
        fasta_length = fasta_seq.length();

        fasta_seq_orig =fasta_seq;

        gettimeofday(&start,NULL);
        reverse(fasta_seq.begin(),fasta_seq.end());
        fmindex.transform(&fasta_seq);//pass pointer of s and transform to BWT String
    }
    cout << "Starting build FM index..." << endl;
    fmindex.buildIndex(); // Build FM-Index for Searching
    cout << "Done" << endl;
    gettimeofday(&finish,NULL);
    duration=finish.tv_sec-start.tv_sec+(finish.tv_usec-start.tv_usec)/1000000.0;
    cout<<"BWT construction time: "<<duration<<" s"<<endl;


//    reverse(fasta_seq.begin(),fasta_seq.end());
   
    fmint_t bwt_str_size=0,bwt_str_capacity=0,sa_size=0,sa_capacity=0,occ_size=0,C_size=0,C_capacity=0;
    bwt_str_size=sizeof(char)*fmindex.returnBWTString().size();
    bwt_str_capacity=sizeof(char)*fmindex.returnBWTString().capacity();
    sa_size=sizeof(fmint_t)*fmindex.returnSA().size();
    sa_capacity=sizeof(fmint_t)*fmindex.returnSA().capacity();
    occ_size=sizeof(tr1::unordered_map<char,fmint_t>::value_type)*fmindex.returnOcc().size();
    //cout << "sizeof(tr1::unordered_map<char,fmint_t>::value_type) " << sizeof(tr1::unordered_map<char,fmint_t>::value_type) << endl;
    for(fmint_t i=0;i<(fmint_t)fmindex.returnCheckpoints().size();i++)
    {
        //cout << "fmindex.returnCheckpoints()[i].size(): " << fmindex.returnCheckpoints()[i].size() << endl;
        C_size+=sizeof(tr1::unordered_map<char,fmint_t>::value_type)*fmindex.returnCheckpoints()[i].size();
    }



    cout<<"bwt_str_size: "<<(bwt_str_size)<<" bytes"<<endl;
    cout<<"sa_size: "<<(sa_size)<<" bytes"<<endl;
    cout<<"occ_size: "<<(occ_size)<<" bytes"<<endl;
    cout<<"C_size: "<<(C_size)<<" bytes"<<endl;




    FMIndex *fmidx;
    fmidx = &fmindex;
    fmint_t k,sub;
    vector<double>running_time;
    vector<double>running_time_foreach_k_phase1;
    vector<double>running_time_foreach_k_phase2;
    vector<fmint_t> ncheckingpoints_foreach_k;
    vector<int> ncheckingregion_length_foreach_k;
    vector<double>running_time_phase1;
    vector<double>running_time_phase2;
    vector<fmint_t> ncheckingpoints;
    vector<fmint_t> nodes;
    vector<int> ncheckingregion_length;
    
    int no_reads;
    cout << "Number of reads = ";
	cin >> no_reads;
    /*
    cout << "Number of subpatterns l = ";
	cin >> sub;
    */
    
    vector<fmint_t> checkpoints;
    vector<Text_intervals> sub_intevals;
    vector<fmint_t> total_checking_points;
    fmint_t total_nodes = 0;
    nodes.clear();
    for (fmint_t err = 1; err <= 7; err += 1)
    {
        gzFile fp_fq;
        kseq_t *read;
        fp_fq = gzopen(fastq_file.c_str(), "r");
        read = kseq_init(fp_fq);
        count = 0;
        k = err;
        gettimeofday(&start,NULL);
        fmint_t check = 0;
        EditDistanceTree edistTree;
        total_nodes = 0;
        cout << "k differences: " << k << endl;
        while ((l = kseq_read(read)) >= 0)
        {
            count++;
            cout << "Read Sequence: " << read->seq.s << endl;
            cout << "Read Sequence Length: " << read->seq.l << ", Read #" << count << endl;
            string q(read->seq.s);
            k = err;
            edistTree.construct(fmidx, fasta_length, q, k, total_nodes);
            if (count == no_reads)
                break;
        }
        gettimeofday(&finish,NULL);
        duration=finish.tv_sec-start.tv_sec+(finish.tv_usec-start.tv_usec)/1000000.0;
        running_time.push_back(duration);
        cout << "total_nodes: " << total_nodes << endl;
        nodes.push_back(total_nodes/no_reads);
        cout<<"EBWT searching time: "<<duration<<" s"<<endl;
        cout<<"read amount: "<<count<<endl;

        kseq_destroy(read);
        gzclose(fp_fq);
    }

    for (int l = 0; l < running_time.size(); l++){
        cout << "k = " << l + 1 << ", running time: " << running_time[l] << " total_nodes: " << nodes[l] << endl;
    }
    
    return 0;
}

