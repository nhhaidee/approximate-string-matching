#ifndef EDTree_H_
#define EDTree_H_

#include <string>
#include <vector>
#include <iostream>
#include <stack>
#include <map>
#include <tr1/unordered_map>
#include <sys/time.h>
//#include "bwt.h"
#include "DataTypes.h"


using namespace std;


//////////////// Class EditDistanceTree //////////////////////////////
class EditDistanceTree{
    private:
        FMIndex* fmindex;
        //int m = 40;
        //int k = 4;
        //GATCGATCGATCGTAGCTAGC
        //TGTTTAAGTACAAAACCCTCAATGCTTTAGCTGCCCAGTATTTGAGGGAGCTCCTGGTGT
        //int D[76][84];
        //int L[76][84];
        //TGGATGTCCACCATTCTCCAGTCTCCGGTGCCTAGACTGCAGCTCTGTACAGTAAGTCTGGCCAGAGGAGAAATGGTCATGCCCAACTGAGCCTGGTTTGGAGGTTATTTTTCCCCTTTA
        int **D;
        int **L;
        vector<fmint_t> edmatches;//query results
        vector<fmint_t> edsearches;
        vector<fmint_t> results;
        vector<fmint_t> scanningresults;
        array<fmint_t, 1000>boundstack; // (m + k + 2)*4 = (200 + 20 + 2)*4 = 888 ~ 1000
        array<int, 305> klevels;
        array<int, 305> klevels_index;
        int stack_level;
        map <u_int8_t, char> Nucleotide = {
            {0, 'Y'},
            {1, 'W'},
            {2, 'V'},
            {3, 'T'},
            {4, 'S'},
            {5, 'R'},
            {6, 'Q'},
            {7, 'P'},
            {8, 'N'},
            {9, 'M'},
            {10, 'L'},
            {11, 'K'},
            {12, 'I'},
            {13, 'H'},
            {14, 'G'},
            {15, 'F'},
            {16, 'E'},
            {17, 'D'},
            {18, 'C'},
            {19, 'A'}
        };
    public:
        EditDistanceTree();
       
        ~EditDistanceTree();
        ////// Stack Control ///////
        inline void pushBound(char ch, fmint_t first, fmint_t last, fmint_t dis)
        {
            stack_level = stack_level + 1;
            boundstack[4*stack_level + 0] = encode_ch(ch);
            boundstack[4*stack_level + 1] = first;
            boundstack[4*stack_level + 2] = last;
            boundstack[4*stack_level + 3] = dis;
        }

        inline void popBound()
        {
            stack_level = stack_level - 1;
        }

        bool isEmpty()
        {
            if (stack_level == -1)
                return true;
            else
                return false;
        }
        vector<fmint_t> findbound(FMIndex *fmidx,char& q,fmint_t& top,fmint_t& bot);

        vector<fmint_t> ed_walk(FMIndex *fmidx,fmint_t& top,fmint_t& bot);

        void construct(FMIndex *fmidx, fmint_t fasta_length, string subpatterns, int errors, fmint_t &total_nodes);

        vector<fmint_t> scanning(FMIndex *fmidx, fmint_t fasta_length, string& subpatterns, int errors)
        {
            this->fmindex = fmidx;
            scanningresults.clear();
            fmint_t first;
            fmint_t last;
            int viableprefix_len = 0;
            int read_len = subpatterns.length();
            int largest_index = 0;
            int depth;
            int k_errors;
            int k_top;
            char c;
            int m = subpatterns.length();
            //cout << "Sub patterns " << subpatterns << " " << subpatterns[0] << " " << subpatterns[1] <<endl;
            klevels.fill(errors+1);
            klevels_index.fill(errors+1);
            stack_level = -1;
            int total_nodes = 0;
            boundstack.fill(0);

            // Initalize matrix;
            int row = m +1;
            int col = m + errors + 2;
            int i, j;
            
            D = new int*[row];
            L = new int*[row];
            for (int i = 0; i < row; i++)
            {
                D[i] = new int[col];
                L[i] = new int[col];
            }
            
            for (i = 0; i < m + errors + 2; i++)
            {
                for (j = 0; j < m + 1; j++)
                {
                    D[j][i] = j;
                    L[j][i] = 0;
                }
            }
            pushBound('-', 0, fmindex->returnBWTStringLength(), 0); // Root node
            while(stack_level != -1)
            {
                popBound(); // stack_level decrement by 1
                c = decode_ch(boundstack[4*(stack_level + 1) + 0]); 
                //cout << "C Bound: " << c << std::endl;
                first = boundstack[4*(stack_level + 1) + 1];
                last = boundstack[4*(stack_level + 1) + 2];
                depth = boundstack[4*(stack_level + 1) + 3];
                //cout << "k: " << errors << "," << c << ", first: " << first << ", last:" << last << ", depth:"<< depth << std::endl;
                
                if (depth > 0) // dist_level = 0 which is root
                {
                    k_top = klevels[depth -1];
                    edp(depth, subpatterns, c, k_top, errors);
                    largest_index = klevels_index[depth];
                    viableprefix_len = L[largest_index][depth];
                    total_nodes += 1;
                    if (largest_index == read_len)
                    {
                        results.clear();
                        results=ed_walk(fmidx, first, last);
                        k_errors = D[largest_index][depth];
                        for(auto r_it=results.begin();r_it!=results.end();++r_it)
                        {
                            //cout << "Result Pos:" << (fasta_length - *r_it -1) << endl;
                            scanningresults.push_back((fasta_length - *r_it -1)); 
                            cout<< "Pos_Rev: "<< *r_it \
                            << ", Ending Pos: " << (fasta_length - *r_it -1) \
                            << ", k: " << k_errors \
                            << ", Substring length: " << viableprefix_len << endl;                
                        }
                    }
                    
                }
                if (viableprefix_len == depth) // depth = 0 which is root node
                {
                    for(map<u_int8_t,char>::iterator it=Nucleotide.begin(); it!=Nucleotide.end(); ++it)
                    {
                        edsearches.clear();
                        edsearches = findbound(fmidx, it->second, first, last);
                        if (edsearches.size() != 0)
                        {
                            pushBound(it->second, edsearches[0], edsearches[1], depth + 1);
                            
                            if (stack_level >= 250)
                            {
                                cout << "Out of stack" << endl;
                            }
                        }
                    } 
                }
            }
            for (int i = 0; i < m + 1; i++)
            {
                delete[] D[i];
                delete[] L[i];
            }
            delete[] L;
            delete[] D;
            L = NULL;
            D = NULL;
            //cout << "Total Nodes = " << total_nodes << endl;
            return scanningresults;
        }

        void dp(int depth, string& q, char c);
        void edp(int depth, string& q, char c, int k_top, int error);
        int max_index(int depth);
        int emax_index(int depth);

        u_int8_t encode_ch(char &c)
        {
            u_int8_t i;
            switch(c)
            {
                case 'A': i=0;break;
                case 'C': i=1;break;
                case 'D': i=2;break;
                case 'E': i=3;break;
                case 'F': i=4;break;
                case 'G': i=5;break;
                case 'H': i=6;break;
                case 'I': i=7;break;
                case 'K': i=8;break;
                case 'L': i=9;break;
                case 'M': i=10;break;
                case 'N': i=11;break;
                case 'P': i=12;break;
                case 'Q': i=13;break;
                case 'R': i=14;break;
                case 'S': i=15;break;
                case 'T': i=16;break;
                case 'V': i=17;break;
                case 'W': i=18;break;
                case 'Y': i=19;break;
                default: i=20;break; // Root node
            }
            return (u_int8_t)i;
        }

        char decode_ch(u_int8_t i)
        {
            char c;
            switch(i)
            {
                case 0: c='A';break;
                case 1: c='C';break;
                case 2: c='D';break;
                case 3: c='E';break;
                case 4: c='F';break;
                case 5: c='G';break;
                case 6: c='H';break;
                case 7: c='I';break;
                case 8: c='K';break;
                case 9: c='L';break;
                case 10: c='M';break;
                case 11: c='N';break;
                case 12: c='P';break;
                case 13: c='Q';break;
                case 14: c='R';break;
                case 15: c='S';break;
                case 16: c='T';break;
                case 17: c='V';break;
                case 18: c='W';break;
                case 19: c='Y';break;
                default: c='-';break; // Root node
            }
            return c;
        }
};
///////////////////////// End Class ///////////////////////////

#endif