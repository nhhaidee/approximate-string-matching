#ifndef SuffixTree_H
#define SuffixTree_H

#include<iostream>
#include<string>
#include<vector>
#include <array>
#include <unordered_map>
#include <algorithm>
#include <iostream>
#include <fstream>
#include "math.h"
#include "DataTypes.h"


using namespace std;
class SuffixTree
{   
    private:

        struct Intervals
        {
            clint_t substr_start;
            clint_t substr_end;
        };

        struct SuffixTreeNode 
        {
            SuffixTreeNode *children[MAX_CHILD];
            SuffixTreeNode *suffixLink;
            int start;
            int *end;
            int substr_len; // Label length of substring (from node to root)
            //int level; //Level of each node in suffix tree
            int id; // unique id for each node in suffix tree which using for euler tour
            clint_t suffixIndex; // Internal node has suffix index is -1
        };
        typedef SuffixTreeNode Node;   

        Node *newNode(int start, int *end)
        {
            Node * node = new Node;
            int i;
            for (i = 0; i < MAX_CHILD; i++)
                node->children[i] = NULL;
            node->suffixLink = root;
            node->start = start;
            node->end = end;
            node->suffixIndex = -1;
            node->substr_len = 0;
            //node->level = 0;
            node->id = 0;
            return node;
        }

        Node* root;
        Node* lastNewNode;
        Node* activeNode;
        int remainingSuffixCount;
        int leafEnd;
        int activeEdge;
        int activeLength;
        int *rootEnd;
        int *splitEnd;
        clint_t max_jump;
        //variable utils
        int total_nodes; // Total of nodes in suffix tree
        //array<Node*, 2*total_nodes -1>euler;
        vector<Node*> euler;
        vector<int> node_level;
        array<int, 3000> first_occ; // First of occurrence of node during euler tour
        int ind;
        //unordered_map<int, Node*> suffixTreeNode; // Hash table to store pair of (nodeid----> node add) just for testing
        unordered_map<int, Node*> leafNode;
        //vector<int> M;
        //vector<Node*> M1;
        int **lookup_table;
        clint_t **L;
        Node** M1;
        clint_t* M;
        vector<int>checking_point;
        ofstream out_file;


    public:

        SuffixTree();
       ~SuffixTree();
        //////////////////////////Functions for suffix tree of pattern////////////////////////////
        int edgeLength(Node *n);
        int walkDown(Node *currNode);
        void extendSuffixTree(int pos, string& pattern);
        void setSuffixIndexByDFS(Node *n, int labelHeight, string& T);
        void setEdgeLength();
        void freeSuffixTree(Node *n);
        void buildSuffixTree(string& pattern);
        void free_memory_usage()
        {
            for (uint i = 0; i < node_level.size() ; i++)
                delete [] lookup_table[i];
            delete[] lookup_table;
            lookup_table = NULL;
            freeSuffixTree(root);
            delete []M;
            delete []M1;
            M = NULL;
            M1 = NULL;
        }
        /////////////////////////////////////////////////////////////////////////////////////////


        /////////////////////////// Function for matching statistic//////////////////////////////
        void matchingStatistics(string &x, string &y);
        int len(Node* r, char c); 
        int first(Node* r, char c);
        Node *child(Node* r, char c) // Find children of a node which using for matching statistic
        {
            if (r->children[encode(c)] != NULL)
                return r->children[encode(c)];
            else 
                return NULL;  
        }
        ////////////////////////////////////////////////////////////////////////////////////////

        ////////////////////////// Functions for finding LCA of suffix Tree////////////////////
        void eulerTour(Node *n, int l);
        void RMQpreprocess(vector<int>node_level)
        {
            int col;
            int size = node_level.size();
            col = log2(size) + 1;

            lookup_table = new int*[size];
            for (int i = 0; i < size ; i++)
                lookup_table[i] = new int[col];
            
            for (int i = 0; i < size; i++)
                lookup_table[i][0] = i;
 
            for (int j=1; (1<<j)<=size; j++)
            {
            // Compute minimum value for all intervals with size 2^j
                for (int i=0; (i+(1<<j)-1) < size; i++)
                { 
                    // For arr[2][10], we compare arr[lookup[0][3]] and
                    // arr[lookup[3][3]]
                    if (node_level[lookup_table[i][j-1]] < node_level[lookup_table[i + (1<<(j-1))][j-1]])
                        lookup_table[i][j] = lookup_table[i][j-1];
                    else
                        lookup_table[i][j] = lookup_table[i + (1 << (j-1))][j-1];      
                }
            }
        }
        int RMQlookup(int qs, int qe)
        {
            
            int j = (int)log2(qe-qs+1);
            
            if (node_level[lookup_table[qs][j]] <= node_level[lookup_table[qe - (1<<j) + 1][j]])
                return lookup_table[qs][j];
            else return lookup_table[qe - (1<<j) + 1][j];

        }
        clint_t LCA(Node* u, Node* v) // Return LCA node of node u and v
        {
            clint_t qs;
            clint_t qe;
            if(u == v)
                return u->substr_len;
            //cout << root << " "  << u << " " << v << endl;
            if(first_occ[u->id] > first_occ[v->id])
            {
                qs = first_occ[v->id];
                qe = first_occ[u->id];
            }
            else
            {
                qs = first_occ[u->id];
                qe = first_occ[v->id];
            }
            //cout << "qs = " << qs << ", qe = "  << qe << endl; 
            //cout << "u->id " << u->id << ", v->id " << v->id << endl; 
            //Normal way to do, reduce LCA for RMQ 
            /*
            int min =node_level[qs];;
            int index = qs;
            for (int i = qs; i <= qe; i++)
            {
            if (min >= node_level[i])
            {
                min = node_level[i];
                index = i;
            }
            }
            return euler[index]->substr_len;
            */
        // cout << euler.size() << " " << euler[RMQlookup(qs,qe)] << endl;
        //cout << RMQlookup(qs,qe) << endl;
            return euler[RMQlookup(qs,qe)]->substr_len;
        }
        /////////////////////////////////////////////////////////////////////////////////////////////
        // Util functions
        u_int8_t encode(char &c)
        {
            u_int8_t i;
            switch(c)
            {
                case 'A': i=0;break;
                case 'C': i=1;break;
                case 'D': i=2;break;
                case 'E': i=3;break;
                case 'F': i=4;break;
                case 'G': i=5;break;
                case 'H': i=6;break;
                case 'I': i=7;break;
                case 'K': i=8;break;
                case 'L': i=9;break;
                case 'M': i=10;break;
                case 'N': i=11;break;
                case 'P': i=12;break;
                case 'Q': i=13;break;
                case 'R': i=14;break;
                case 'S': i=15;break;
                case 'T': i=16;break;
                case 'V': i=17;break;
                case 'W': i=18;break;
                case 'Y': i=19;break;
                case '$': i=20;break;
                default:  i=21;break; //Other characters
            }
            return (u_int8_t)i;
        }
        clint_t jump(clint_t i, clint_t j);//J(i, j) = min{M[j], LCA(M'[j], suffix x(i,m)&)),j is index of matching statistics array, i is the suffix index of pattern
        void MatrixL(int read_len, int k, clint_t substring_len);
        void MatrixL_let_set(int& read_len, int& k, clint_t& start_pos, clint_t& end_pos, clint_t& fasta_len);
        void mergeIntervals(vector<Intervals>& sub_interval, int& read_len, int& k,clint_t& fasta_len);
        void changlawler_let(clint_t& fasta_len,int& read_len, int& k);
        int transformIndex(clint_t x)
        {
            return x - 2;
        }
        int maximum(clint_t i, clint_t j, clint_t k)
        {
            return max(max(i,j),k);
        }
        void initialize_L(int k, int substring_len)
        {   
            L = new clint_t*[k+1];
            for (clint_t i = 0; i <=k ; i++)
                L[i] = new clint_t[2*(substring_len+2)];
            for (clint_t j = 0; j <= k; j++)
                for (clint_t l =0; l <= 1; l++)
                    L[j][l] = -1;
        }  
        void changlawler_set (clint_t& fasta_len,int& read_len, int& k)
        {
            clint_t i, j, end_pos;
            clint_t set_jump;
            vector<Intervals> sub_interval;
            clint_t genome_len = fasta_len;
            clint_t subpattern_len = (read_len - k)/2;
            sub_interval.clear();
            sub_interval.shrink_to_fit();
            Intervals l;
            for (i = 0; i < genome_len; i += subpattern_len)
            {
                set_jump = 0;
                j = 0;
                end_pos = 0;
                do
                {
                    set_jump = set_jump + M[i+set_jump]+1;
                    j = j + 1;
                    if (set_jump > subpattern_len-1)
                    {
                        end_pos = set_jump + i;
                        break;
                    }
                }while(j <= k && (i + set_jump) <= fasta_len); // Compute k + 1 maximum jump
                if (end_pos != 0)
                {
                    
                    if ((i - (read_len + 3*k)/2) < 0)
                        l.substr_start = 0;
                    else
                        l.substr_start = i - (read_len + 3*k)/2;
                    if (end_pos > fasta_len)
                        end_pos = fasta_len;
                    l.substr_end = end_pos;
                    sub_interval.push_back(l);
                }
            }
            if (sub_interval.size()>0)
            {
              //  cout << "Total Checking Region: " << sub_interval.size() << endl;
                mergeIntervals(sub_interval, read_len, k,fasta_len);
            }
            /*
            else
            {
                cout << "No Matching Regions Found " << endl;
            }
            */
        }
};
#endif
