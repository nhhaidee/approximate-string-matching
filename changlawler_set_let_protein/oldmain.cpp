
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sys/time.h>
#include <string>
#include <zlib.h>
#include "kseq.h"
#include "SuffixTree.h"
#include "DataTypes.h"
#include <unordered_map>

KSEQ_INIT(gzFile, gzread)

using namespace std;

int main()
{

    struct timeval start,finish;
    struct timeval MS_start,MS_finish;
    struct timeval checking_le_start,checking_le_finish;
    struct timeval checking_set_start,checking_set_finish;
    int l;
    double duration;

     //////////////////// Reference Sequence /////////////////
    string c_merrolae_fasta="/home/nguyen-h85/Desktop/Source_Code/Reference_Sequence/Cyanidioschyzon_merolae.ASM9120v1.dna.chromosome.1.fa";
    string danio_rerio_fasta="/home/nguyen-h85/Desktop/Source_Code/Reference_Sequence/Danio_rerio.GRCz11.dna.chromosome.1.fa";
    string drosophila_melanogaster_fasta="/home/nguyen-h85/Desktop/Source_Code/Reference_Sequence/Drosophila_melanogaster.BDGP6.dna.chromosome.2L.fa";
    string gorilla_fasta="/home/nguyen-h85/Desktop/Source_Code/Reference_Sequence/Gorilla_gorilla.gorGor4.dna.fa";
    string gorilla_chr1_fasta="/home/nguyen-h85/Desktop/Source_Code/Reference_Sequence/Gorilla_gorilla.gorGor4.dna.chromosome_1.fa";
    /////////////////////////////////////////////////////////
    string c_merrolae_reads_100bps="/home/nguyen-h85/Desktop/Source_Code/reads/Cyanidioschyzon_merolae_5000_reads_100bps.fq";
    string c_merrolae_reads_300bps="/home/nguyen-h85/Desktop/Source_Code/reads/Cyanidioschyzon_merolae_5000_reads_300bps.fq";
    string danio_rerio_reads_100bps="/home/nguyen-h85/Desktop/Source_Code/reads/Danio_rerio_5000_reads_100bps.fq";
    string danio_rerio_reads_300bps="/home/nguyen-h85/Desktop/Source_Code/reads/Danio_rerio_5000_reads_300bps.fq";
    string drosophila_melanogaster_reads_100bps="/home/nguyen-h85/Desktop/Source_Code/reads/Drosophila_melanogaster_5000_reads_100bps.fq";
    string drosophila_melanogaster_reads_300bps="/home/nguyen-h85/Desktop/Source_Code/reads/Drosophila_melanogaster_5000_reads_300bps.fq";
    string gorilla_reads_100bps="/home/nguyen-h85/Desktop/Source_Code/reads/Gorilla_gorilla_5000_reads_100bps.fq";
    string gorilla_reads_300bps="/home/nguyen-h85/Desktop/Source_Code/reads/Gorilla_gorilla_5000_reads_300bps.fq";
    string gorilla_chr1_reads_100bps="/home/nguyen-h85/Desktop/Source_Code/reads/Gorilla_gorilla_chr1_5000_reads_100bps.fq";
    string gorilla_chr1_reads_300bps="/home/nguyen-h85/Desktop/Source_Code/reads/Gorilla_gorilla_chr1_5000_reads_300bps.fq";
    vector <string>fasta_ref;

    
    fasta_ref.clear();

    fasta_ref.push_back(c_merrolae_fasta);
    fasta_ref.push_back(drosophila_melanogaster_fasta);
    fasta_ref.push_back(gorilla_chr1_fasta);
    fasta_ref.push_back(danio_rerio_fasta);
    fasta_ref.push_back(gorilla_fasta);

    vector <string>fastq_reads_0;
    fastq_reads_0.push_back(c_merrolae_reads_100bps);
    fastq_reads_0.push_back(c_merrolae_reads_300bps);

    vector <string>fastq_reads_1;
    fastq_reads_1.push_back(drosophila_melanogaster_reads_100bps);
    fastq_reads_1.push_back(drosophila_melanogaster_reads_300bps);

    vector <string>fastq_reads_2;
    fastq_reads_2.push_back(gorilla_chr1_reads_100bps);
    fastq_reads_2.push_back(gorilla_chr1_reads_300bps);

    vector <string>fastq_reads_3;
    fastq_reads_3.push_back(danio_rerio_reads_100bps);
    fastq_reads_3.push_back(danio_rerio_reads_300bps);

    vector <string>fastq_reads_4;
    fastq_reads_4.push_back(gorilla_reads_100bps);
    fastq_reads_4.push_back(gorilla_reads_300bps);

    std::unordered_map <int, vector<string>> fastq_files;

    fastq_files[0] = fastq_reads_0;
    fastq_files[1] = fastq_reads_1;
    fastq_files[2] = fastq_reads_2;
    fastq_files[3] = fastq_reads_3;
    fastq_files[4] = fastq_reads_4;

    for (int i = 0; i <=fasta_ref.size() -1; i++)
    {
        std::cout << "Running test: " << fasta_ref[i] << std::endl;

        string fasta_file = fasta_ref[i];
        gzFile fp_fa;
        kseq_t *seq;
        fp_fa = gzopen(fasta_file.c_str(), "r");
        seq = kseq_init(fp_fa);   
        string fasta_seq;
        while ((l = kseq_read(seq)) >= 0)
        {
            fasta_seq.append(seq->seq.s);
            fasta_seq.push_back('%');
        }
        DNA_Translate(&fasta_seq);
        std::cout << "Genome Length: " << fasta_seq.length() << std::endl;
        kseq_destroy(seq);
        gzclose(fp_fa);

        vector<string> reads_file;
        reads_file = fastq_files.at(i);

        //vector <double>running_time_LET;
        //vector <double>running_time_SET;
        for (int j = 0; j  < 2; j++)
        {
            std::cout  << "Aligning: ";
           // running_time_LET.clear();
            //running_time_SET.clear();
            if (j == 0) // 100 bps long
            {
                std::cout << reads_file[j] << std::endl;
                
                for (int cl = 0; cl <=1; cl++)
                {
                    for (int err = 1; err <= 2; err++)
                    {
                        int k;
                        int count = 0;
                        gzFile fp_fq;
                        kseq_t *read;
                        string pattern;
                        fp_fq = gzopen(reads_file[j].c_str(), "r");
                        
                        read = kseq_init(fp_fq);
                        k = err;
                        clint_t fasta_seq_len = fasta_seq.length()-1;
                        int read_len;
                        gettimeofday(&start,NULL);
                        SuffixTree stree;
                        double total_ms_time = 0;
                        double total_checking_let_time = 0;
                        double total_checking_set_time = 0;
                        //cout << "Here" << endl;
                        while ((l = kseq_read(read)) >= 0)
                        {
                            count++;
                            //cout << "Read Sequence Length: " << read->seq.l << ", Read No: " << count << endl;
                            gettimeofday(&MS_start,NULL);
                            string q(read->seq.s);   
                            pattern = q;
                            pattern.push_back('$'); 
                            //cout << "Pattern: " << pattern << endl;
                            read_len = pattern.length()-1;  
                            stree.buildSuffixTree(pattern);
                            //cout << "Done Suffix Tree " << endl;
                            stree.matchingStatistics(pattern, fasta_seq);
                            //cout << "Done Matching statistics " << endl;
                            gettimeofday(&MS_finish,NULL);
                            double duration_ms=MS_finish.tv_sec-MS_start.tv_sec+(MS_finish.tv_usec-MS_start.tv_usec)/1000000.0;
                            total_ms_time = total_ms_time + duration_ms;
                            if (cl == 0)
                            {
                                gettimeofday(&checking_le_start,NULL);
                                stree.changlawler_let(fasta_seq_len, read_len, k);
                                gettimeofday(&checking_le_finish,NULL);
                                double duration_checking_let=checking_le_finish.tv_sec-checking_le_start.tv_sec+(checking_le_finish.tv_usec-checking_le_start.tv_usec)/1000000.0;
                                total_checking_let_time = total_checking_let_time + duration_checking_let;
                            } 
                            else 
                            {
                                gettimeofday(&checking_set_start,NULL);
                                stree.changlawler_set(fasta_seq_len, read_len, k);
                                gettimeofday(&checking_set_finish,NULL);
                                double duration_checking_set=checking_set_finish.tv_sec-checking_set_start.tv_sec+(checking_set_finish.tv_usec-checking_set_start.tv_usec)/1000000.0;
                                total_checking_set_time = total_checking_set_time + duration_checking_set;
                            }
                            
                            if (count ==  100)
                                break;
                        }
                        gettimeofday(&finish,NULL);
                        cout<<"read amount: "<<count<<endl;
                        kseq_destroy(read);
                        gzclose(fp_fq);
                        duration=finish.tv_sec-start.tv_sec+(finish.tv_usec-start.tv_usec)/1000000.0;

                        if (cl == 0){
                            cout << "LET:" << ", k: " << k << ", MS_Time: " << total_ms_time << ", Checking_Time: " << total_checking_let_time << ", Total_Time: " << duration << endl;
                        }
                        else{
                            cout << "SET:" << ", k: " << k << ", MS_Time: " << total_ms_time << ", Checking_Time: " << total_checking_set_time << ", Total_Time: " << duration << endl;
                        }
                            
                    }
                }
                stree.free_memory_usage();///
                
            }
            /////////////// Don't touch///////////////////////   
            else // Align reads with 300 bps
            {
                std::cout << reads_file[j] << std::endl;
            }
        }
        fasta_seq.clear();
        fasta_seq.shrink_to_fit();
        if (i == 1)
            break;

    }






    return 0;
}
