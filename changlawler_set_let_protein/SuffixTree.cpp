#include"SuffixTree.h"
#include<string>
#include <stack>
#include<iostream>
#include<algorithm>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include "DataTypes.h"

using namespace std;

SuffixTree::SuffixTree()
{
   // string out_path="./out.txt";
   // out_file.open(out_path,ios::out);
}

SuffixTree::~SuffixTree()
{
  //  out_file.close();   
}

int SuffixTree::edgeLength(Node *n) {
    if (n==root)
    {
        return 0;
    }
    else
    {
        return *(n->end) - (n->start) + 1;
    }
    
}

int SuffixTree::walkDown(Node *currNode)
{
    if (activeLength >= edgeLength(currNode))
    {
        activeEdge += edgeLength(currNode);
        activeLength -= edgeLength(currNode);
        activeNode = currNode;
        return 1;
    }
    return 0;
}

void SuffixTree::extendSuffixTree(int pos, string& pattern)
{

    leafEnd = pos;
    remainingSuffixCount++;
 
    lastNewNode = NULL;

    while(remainingSuffixCount > 0) 
    {
        if (activeLength == 0)
            activeEdge = pos; //APCFALZ
        if (activeNode->children[encode(pattern[activeEdge])] == NULL)
        {          
            activeNode->children[encode(pattern[activeEdge])] = newNode(pos, &leafEnd);            
            if (lastNewNode != NULL)
            {
                
                lastNewNode->suffixLink = activeNode;
                lastNewNode = NULL;
            }
        }
        else
        {       
            Node *next = activeNode->children[encode(pattern[activeEdge])];
            if (walkDown(next))//Do walkdown
            {
                continue;
            }
            if (pattern[next->start + activeLength] == pattern[pos])
            {
                if(lastNewNode != NULL && activeNode != root)
                {
                    lastNewNode->suffixLink = activeNode;
                     
                    lastNewNode = NULL;
                }
                //APCFER3
                activeLength++;
                break;
            }
 
            splitEnd = new int;
            *splitEnd = next->start + activeLength - 1;
 
            //New internal node
            Node *split = newNode(next->start, splitEnd);
            activeNode->children[encode(pattern[activeEdge])] = split;
 
            //New leaf coming out of new internal node
            split->children[encode(pattern[pos])] = newNode(pos, &leafEnd);
            next->start += activeLength;
            split->children[encode(pattern[next->start])] = next;

            if (lastNewNode != NULL)
            {
                lastNewNode->suffixLink = split;
            }
 
            lastNewNode = split;
        }
        
        remainingSuffixCount--;
        if (activeNode == root && activeLength > 0) //APCFER2C1
        {
            activeLength--;
            activeEdge = pos - remainingSuffixCount + 1;
        }
        else if (activeNode != root) //APCFER2C2
        {
            activeNode = activeNode->suffixLink;
        }
    }
}

void SuffixTree::buildSuffixTree(string& T)
{ 
    
    //Initialize for building suffix tree
    root = NULL;
    rootEnd = new int;
    *rootEnd = - 1;
    root = newNode(-1, rootEnd); 
   // cout <<" Root " << root << endl;
   // cout << root->children[0] << " " <<root->children[1] << " " << root->children[2] << " " << root->children[3] << endl;
    activeNode = NULL;
    activeEdge = -1;
    activeLength = 0;
    remainingSuffixCount = 0;
    leafEnd = -1;
    splitEnd = NULL;
    activeNode = root; //First activeNode will be root
    //cout << "Root: " << root << " Root ID: " << root->id << endl;
    // Initialize for finding LCA using RMQ Technique
    euler.clear();
    euler.shrink_to_fit();
    node_level.clear();
    node_level.shrink_to_fit();
    ind = 0;
    leafNode.clear();
    // Building suffix tree for pattern
    for (uint i=0; i< T.length() ; i++)
    {
        extendSuffixTree(i, T);
    }
    int labelHeight = 0;
    setEdgeLength();
    setSuffixIndexByDFS(root, labelHeight, T);
    /*
    for (auto x:leafNode)
    {
        cout << x.second <<  " suffix index: " << x.second->suffixIndex << ", sub len: " << x.second->substr_len << endl;
    }
    */
    // Preproessing for fining LCA in suffix tree of pattern
    first_occ.fill(-1);
    eulerTour(root,0);
    RMQpreprocess(node_level);
    /*
    for (int i = 0; i <euler.size();i++)
        cout <<  euler[i] << " Node ID: " << euler[i]->id <<  " depth:" << node_level[i] << endl; ;
    for (int i = 0; i <total_nodes; i++ )
        cout << first_occ[i] << " " << endl;
    cout << endl;
    cout << "Total of nodes: " << total_nodes << endl;

    // Test LCA
    //Node* start = SuffixTreeNode.find('5');
    */
    //cout << "LCA Length: " << LCA(leafNode.at(3), leafNode.at(1)) << endl;
    //cout << start << " " << start->id <<  " " << start->substr_len<< endl;
    //freeSuffixTree(root); // Free memory the tree 
    //leafNode.clear();// clear Hash Table 
}


void SuffixTree::setEdgeLength()
{
    stack <Node*> stack;
    //suffixTreeNode[root->id]= root;
    stack.push(root);
    int counter = 0;
    while(!stack.empty())
    {
        Node* Parent = stack.top();
        stack.pop();  
        for (int i = 0; i < MAX_CHILD; i++)
        {
            if (Parent->children[i] != NULL)
            {
                counter++;
                Parent->children[i]->substr_len = *Parent->children[i]->end - Parent->children[i]->start + 1 + Parent->substr_len;
                //Parent->children[i]->level = Parent->level + 1; // calculate level of each node
                Parent->children[i]->id = counter; // set unique node ID for each node
                //suffixTreeNode[Parent->children[i]->id] = Parent->children[i];
                stack.push(Parent->children[i]);
            }
        }
    }
}

void SuffixTree::setSuffixIndexByDFS(Node *n, int labelHeight, string& T)
{
    if (n == NULL)  
        return;
    /*
    if (n->start != -1) //A non-root node
    {
        //Print the label on edge from parent to current node
        //print(n->start, *(n->end), T);
        cout << T.substr(n->start,*(n->end) - n->start +1); //edgeString
    }
    */
    int leaf = 1;
    for ( int i = 0; i < MAX_CHILD; i++)
    {
        if (n->children[i] != NULL)
        {
            //total_nodes+=1;
            /*
            if (leaf == 1 && n->start != -1)
            {
               cout <<  " [" << n->suffixIndex << "]"  << ", [" << n->start << ", " << *n->end << "]" << " Node: " << n << " suffixLink: " << n->suffixLink <<  " Edge Length: " << n->substr_len  << " Node ID: " << n->id <<endl; 
            }
            */
            leaf = 0;
            setSuffixIndexByDFS(n->children[i], labelHeight + edgeLength(n->children[i]), T);
        }
    }
    if (leaf == 1)
    {
        n->suffixIndex = T.length() - labelHeight;
        //cout << n->suffixIndex << endl;
        leafNode[n->suffixIndex]= n;//store pair of suffixIndex and Node add to hash table
        //cout <<  " [" << n->suffixIndex << "]"  << ", [" << n->start << ", " << *n->end << "]" << " Node: " << n << " suffixLink: " << n->suffixLink <<  " Edge Length: " << n->substr_len  << " Node ID: " << n->id << endl;
    }
}
int SuffixTree::len(Node* r, char c)
{
    return *r->children[encode(c)]->end - r->children[encode(c)]->start + 1;
}

int SuffixTree::first(Node* r, char c)
{
   return r->children[encode(c)]->start;
}

void SuffixTree::matchingStatistics(string &x, string &y)
{
    Node *r = root;
    Node *oldr;
    //M.clear();
    //M.shrink_to_fit();
    //M1.clear();
    //M1.shrink_to_fit();
    //cout << root->children[0] << " " <<root->children[1] << " " << root->children[2] << " " << root->children[3] << endl;
    clint_t h = 0;
    clint_t l = 0;
    clint_t length = y.length();
    clint_t j;
    M  = new clint_t[length+1];
    M1 = new Node*[length+1];
    //cout <<"TestM: " << M1[560] << " " << M1[1403555] << " " << M1[3063403504] << endl;
    for (j = 0; j < length ; j++)
    {
        while((h < l) && (h + len(r, y[h]) <= l))
        {
            oldr = r;
            r = child(r, y[h]);
            h = h + len(oldr, y[h]);
        }
        if (h == l)
        {
            while((child(r,y[h]) != NULL) && (y[l] == x[first(r,y[h]) + l - h]))
            {
                l = l + 1;
                if (h + len(r,y[h]) == l)
                {
                    r = child(r,y[h]);
                    h = l;
                }
            }
        }
        
        M[j] = l - j;
        //if (j == 307543-99)
            //cout << "Pos " << j << ", " << M[j];
        
        if (h == l)
        {
            M1[j] = r;

        }
        else
        {
            M1[j]= child(r,y[h]);
        }
        
        //if (j == 307543-99)
           // cout << " " << M1[j] << endl;
        if (r == root)
        {
           if (h == l)
           {
               h = h + 1;
               l = l + 1;
           }
           else if (h < l)
           {
               h = h + 1;
           }
        }
        else
        {
            r = r->suffixLink;
        }
    }
}

/*
The below functions for finding LCA in suffix tree of pattern
*/

// Preprocessing for RMQ and LCA
void SuffixTree::eulerTour(Node *n, int l)
{
    if (n)
    {
       euler.push_back(n);
       node_level.push_back(l);
       ind++;
       if (first_occ[n->id] == -1)
          first_occ[n->id] = ind -1;
       for(int i = 0; i < MAX_CHILD; i++)
       {
           if (n->children[i] != NULL)
           {
              eulerTour(n->children[i], l +1);
              euler.push_back(n);
              node_level.push_back(l);  
              ind++;   
           }
       }
    }
}

void SuffixTree::freeSuffixTree(Node* n)
{ //Deallocation memory for re-use in post-order manner
    if (n == NULL)
        return;
    for (int i = 0; i < MAX_CHILD; i++)
    {
        if (n->children[i] != NULL)
            freeSuffixTree(n->children[i]);
    }
    if (n->suffixIndex == -1)
    {
        delete n->end;
        n->end = NULL;
    }
    delete n;
    n = NULL;    
}

clint_t SuffixTree::jump(clint_t i, clint_t j) // j is index of matching statistics array, i is the suffix index of pattern 
{
    //max_jump = min(M[j], LCA(M1[j],leafNode.at(i)));
    //cout << "j = " << j << " " << M1[j] << " " << leafNode.at(i) << endl;
    return  min(M[j], LCA(M1[j],leafNode.at(i)));   
}

void SuffixTree::MatrixL(int read_len, int k, clint_t substring_len)
{

   
   clint_t **L;
   L = new clint_t*[k+1];
   for (clint_t i = 0; i <=k ; i++)
     L[i] = new clint_t[substring_len+2];
   for (clint_t j = 0; j <= k; j++)
     for (clint_t l =0; l <= 1; l++)
        L[j][l] = -1;
   //int m = pattern.length() -1; // Exclude $ after Pattern
   for (clint_t s = 0; s < substring_len+2; s++)
     {
         if (s >= 2)
         {
            L[0][s] = jump(0,transformIndex(s));
           // if (L[0][s] == read_len)
            {
             //   cout << "Ending Pos: " << transformIndex(s) + read_len - 1 << ", k = 0" << endl;
                //out_file<<transformIndex(s) + read_len - 1<<","<<", k = 0"<<" ";
            }
         }
     }
   //cout << "Text Length: " << substring_len << endl;
   for (clint_t s = 2; s < substring_len+2 ; s++)
      for(clint_t q = 1; q <=k; q++)
        { 
            clint_t r;
            r = min(read_len,maximum(L[q-1][s-2], L[q-1][s-1]+1, L[q-1][s]+1));
            if (r + transformIndex(s) - q <= substring_len) // Include matching statistic M[text_len] = 0 which is % after T
            {
                L[q][s] = r + jump(r,r + transformIndex(s) - q);
                if (L[q][s] == read_len)
                {
                    //out_file<<transformIndex(s) - q + read_len - 1<<","<<", k = "<<q<<" ";
                    cout << "Ending Pos: " << transformIndex(s) - q + read_len - 1 << ", k = " << q << endl;
                }               
            }
        } 

    cout << "Matrix L: " <<  endl;
    for (int s = 0; s < substring_len+2; s++)
         cout << transformIndex(s) << " ";
    cout << endl;
    for (int i = 0; i  <= k; i++)
    {
        for (int j = 0; j <substring_len+2; j++)
            cout << L[i][j] << " ";
        cout << endl;
    }

}

void SuffixTree::MatrixL_let_set(int& read_len, int& k, clint_t& start_pos, clint_t& end_pos, clint_t& fasta_length)
{
    clint_t r;
    clint_t substring_len = end_pos - start_pos + 1;
    //Dynamic Allocation memory for matrix L
    //cout << start_pos << " " << end_pos << endl;
    
    L = new clint_t*[k+1];
    for (clint_t i = 0; i <=k ; i++)
        L[i] = new clint_t[substring_len + 2];
    //Initalize for L
    for (clint_t j = 0; j <= k; j++)
       for (clint_t l =0; l <= 1; l++)
           L[j][l] = -1;
    
    for (clint_t s = 2; s < substring_len + 2; s++)
    {
        //cout << s << " " << transformIndex(s) + start_pos << " " <<  M[290094213] << " " << M1[290094213] << endl;
        if (transformIndex(s) + start_pos <=fasta_length)
        {
            L[0][s] = jump(0,transformIndex(s) + start_pos);
            if (L[0][s] == read_len)
                 cout << "Ending Pos: " << transformIndex(s) + start_pos + read_len - 1 << ", k = 0" << endl;
                //out_file<<transformIndex(s) + read_len - 1<<","<<", k = 0"<<" ";
        }
    }
    for (clint_t s = 2; s < substring_len + 2; s++)
    {
        for(clint_t q = 1; q<=k; q++)
        { 
            
            r = min(read_len,maximum(L[q-1][s-2], L[q-1][s-1]+1, L[q-1][s]+1));
            if (r + transformIndex(s) + start_pos - q <= fasta_length) // Include matching statistic M[text_len] = 0 which is % after T
            {
                L[q][s] = r + jump(r,r + transformIndex(s)+ start_pos - q);
                if (L[q][s] == read_len)
                {
                     cout << "Ending Pos: " << transformIndex(s) + start_pos - q + read_len - 1 << ", k = " << q << endl;
                    //out_file<<transformIndex(s) - q + read_len - 1<<","<<", k = "<<q<<" ";
                }
            }
        } 
    } 
    
    for(clint_t i = 0; i < k +1; ++i)
    {
        delete[] L[i];   
    }
    delete[] L;
    L = NULL;
    
}

void SuffixTree::changlawler_let(clint_t& fasta_len,int& read_len, int& k)
{
    vector<clint_t> S;
    vector<Intervals> sub_interval;
    S.clear();
    S.shrink_to_fit();
    S.push_back(0);
    sub_interval.clear();
    sub_interval.shrink_to_fit();
    clint_t tmp = 0;
    clint_t j = 0;
    clint_t i;
    Intervals l;
    do{
        //cout << "S[" << j <<"] = " << S[j] << " " << M[S[j]] << endl;
        tmp = tmp + M[tmp] + 1;
        S.push_back(tmp);
        j = j + 1;
    }while(tmp <= fasta_len);      
    for (i = 0; i < j - 1; i++)
    {
        if ((i + k + 2 <= j) && ((S[i + k + 2] - S[i]) >= (read_len - k)))
        {  
            l.substr_start = S[i];
            l.substr_end = S[i + k + 2] - 1;
            //if (l.substr_end == fasta_seq_len)
                // l.substr_end = l.substr_end -1;
            sub_interval.push_back(l);
            // MatrixL_let(read_len, k, l.substr_start, l.substr_end);
        }
    } 
    S.clear();
    S.shrink_to_fit();
    
    if (sub_interval.size() > 0)
    {
       // cout << "Total Checking Region: " << sub_interval.size() << endl;
        //checking_point.push_back(sub_interval.size());
        mergeIntervals(sub_interval, read_len, k,fasta_len);
    }
    /*
    else
    {
        cout << "No Matching Regions Found " << endl;
    }
    */
    
}

void SuffixTree::mergeIntervals(vector<Intervals>& sub_interval, int& read_len, int& k, clint_t& fasta_len)
{
    stack<Intervals> s;
    s.push(sub_interval[0]);
    for (cluint_t i = 1 ; i < sub_interval.size(); i++)
    {
        Intervals top = s.top();
        if (top.substr_end < sub_interval[i].substr_start)
            s.push(sub_interval[i]);
        else if (top.substr_end < sub_interval[i].substr_end)
        {
            top.substr_end = sub_interval[i].substr_end;
            s.pop();
            s.push(top);
        }
    }
  //  cout << "Out put result: " << endl;;
    int checks = 0;
    while (!s.empty())
    {
        Intervals t = s.top();
        //cout << t.substr_start <<  " " << t.substr_end << " " << read_len << endl;
        MatrixL_let_set(read_len, k, t.substr_start, t.substr_end,fasta_len);
        checks = checks + 1;
        s.pop();
    }
    //cout << "Real checking region: " << checks << endl;
}
