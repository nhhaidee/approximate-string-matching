
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sys/time.h>
#include <string>
#include <zlib.h>
#include "kseq.h"
#include "SuffixTree.h"
#include "DataTypes.h"

KSEQ_INIT(gzFile, gzread)

using namespace std;



int main()
{
    ofstream logile;
    logile.open("runningtime_log.txt");
    struct timeval start,finish;
    struct timeval MS_start,MS_finish;
    struct timeval checking_le_start,checking_le_finish;
    struct timeval checking_set_start,checking_set_finish;
    int l;
    double duration;

     //////////////////// Reference Sequence /////////////////
    string protein_fasta="proteinsequence30M.fa";

    /////////////////////////////////////////////////////////
    string c_merrolae_reads_100bps="proteinreads_1read.fq";

    vector <string>fasta_ref;

    
    fasta_ref.clear();

    fasta_ref.push_back(protein_fasta);


    // Test///
    for (int num_fasta = 0; num_fasta <= 0; num_fasta++)
    {
        ///////////////////// Get each ref sequence////////////////
        
        string fasta_file = "proteinsequence30M.fa"; //Test
        std::cout << "Running test: " << fasta_file << std::endl;
        gzFile fp_fa;
        kseq_t *seq;
        fp_fa = gzopen(fasta_file.c_str(), "r");
        string fasta_seq;
        seq = kseq_init(fp_fa);   
        while ((l = kseq_read(seq)) >= 0)
        {
            fasta_seq.append(seq->seq.s);
            fasta_seq.push_back('%');
        }
        //DNA_Translate(&fasta_seq);
        cout << "-->Genome Length: " << fasta_seq.length() << endl;
        kseq_destroy(seq);
        gzclose(fp_fa);
        ////////////////////////////////////////////////////////
        //vector<string> reads_file;
        string reads_file = "proteinreads_1read.fq";
        int error_max;
        int error_step;
        array<double,10>running_time_LET;
        array<double,10>running_time_SET;
        for (int j = 0; j  <= 0; j++)
        {
            cout  << "-->Aligning: ";
            
            if (j == 0) // 100bps
            {
                cout << reads_file << endl;
                logile << reads_file<< endl;
                error_max = 10;
                error_step = 1;
                int k;
                int count = 0;
                gzFile fp_fq;
                kseq_t *read;
                string pattern;
                fp_fq = gzopen(reads_file.c_str(), "r");
                read = kseq_init(fp_fq);

                clint_t fasta_seq_len = fasta_seq.length()-1;
                int read_len;    
                double total_ms_time = 0;
                running_time_LET.fill(0);
                running_time_SET.fill(0);       
                while ((l = kseq_read(read)) >= 0)
                {
                    count++;

                    SuffixTree stree;
                    //cout << "Read Sequence Length: " << read->seq.l << ", Read No: " << count << endl;
                    string q(read->seq.s);   
                    pattern = q;
                    pattern.push_back('$'); 
                   cout << "-------Read No " << count << "(100bps)--------" << endl;
                   cout << "Pattern: " << pattern << std::endl;
                    read_len = pattern.length()-1;
                    
                    ///////////// Build Suffix Tree and Matching Statistics////////////////////
                    gettimeofday(&MS_start,NULL);

                    stree.buildSuffixTree(pattern);
                    stree.matchingStatistics(pattern, fasta_seq);

                    gettimeofday(&MS_finish,NULL);
                    double duration_ms=MS_finish.tv_sec-MS_start.tv_sec+(MS_finish.tv_usec-MS_start.tv_usec)/1000000.0;
                    total_ms_time = total_ms_time + duration_ms;

                    ///////////////////Checking ////////////////////////////////
                    int y = 0;
                    for (int err =1; err <= error_max; err = err + error_step)
                    {
                        k = err;
                        y++;
                        cout << "k differences: " << err << endl;
                       // cout << "------LET-----" << endl;
                        cout << "------------CL-LET-------------" << endl;
                        gettimeofday(&checking_le_start,NULL);
                        stree.changlawler_let(fasta_seq_len, read_len, k);
                        gettimeofday(&checking_le_finish,NULL);
                        double duration_checking_let=checking_le_finish.tv_sec-checking_le_start.tv_sec+(checking_le_finish.tv_usec-checking_le_start.tv_usec)/1000000.0;
                        running_time_LET[y-1] = running_time_LET[y-1] + duration_checking_let;
                        
                        //cout << "------SET-----" << endl;
                        cout << "------------CL-SET-------------" << endl;
                        gettimeofday(&checking_set_start,NULL);
                        stree.changlawler_set(fasta_seq_len, read_len, k);
                        gettimeofday(&checking_set_finish,NULL);
                        double duration_checking_set=checking_set_finish.tv_sec-checking_set_start.tv_sec+(checking_set_finish.tv_usec-checking_set_start.tv_usec)/1000000.0;
                        running_time_SET[y-1] = running_time_SET[y-1] + duration_checking_set;


                    }
                    stree.free_memory_usage();
                    if (count ==  100)
                        break;

                }

                logile << "Species: " << fasta_ref[num_fasta] <<endl;
                logile << "CL.LET Result" << endl;
                for (int i = 0; i< running_time_LET.size();i++)
                {
                   
                   if (running_time_LET[i] != 0)
                        logile << "k = " << i + 1 << ", checking LET time: " << running_time_LET[i] << ", matching statistics time: " << total_ms_time << endl;
                }
                logile << "CL.SET Result" << endl;
                for (int i = 0; i< running_time_SET.size();i++)
                {
                    if (running_time_SET[i] != 0)
                        logile << "k = " << i + 1 << ", checking SET time: " << running_time_SET[i] << ", matching statistics time: " << total_ms_time << endl;
                }
                kseq_destroy(read);
                gzclose(fp_fq);
            }
            /*
            else // Align reads with 300 bps
            {
                cout << reads_file[j] << endl;
                logile << reads_file[j] << endl;
                error_max = 35;
                error_step = 5;
                int k;
                int count = 0;
                gzFile fp_fq;
                kseq_t *read;
                string pattern;
                fp_fq = gzopen(reads_file[j].c_str(), "r");
                read = kseq_init(fp_fq);

                clint_t fasta_seq_len = fasta_seq.length()-1;
                int read_len;    
                double total_ms_time = 0;
                running_time_LET.fill(0);
                running_time_SET.fill(0);       
                while ((l = kseq_read(read)) >= 0)
                {
                    count++;

                    SuffixTree stree;
                    //cout << "Read Sequence Length: " << read->seq.l << ", Read No: " << count << endl;
                    string q(read->seq.s);   
                    pattern = q;
                    pattern.push_back('$'); 
                   cout << "-------Read No " << count << "(300bps)--------" << endl;
                    read_len = pattern.length()-1;
                    
                    ///////////// Build Suffix Tree and Matching Statistics////////////////////
                    gettimeofday(&MS_start,NULL);

                    stree.buildSuffixTree(pattern);
                    stree.matchingStatistics(pattern, fasta_seq);

                    gettimeofday(&MS_finish,NULL);
                    double duration_ms=MS_finish.tv_sec-MS_start.tv_sec+(MS_finish.tv_usec-MS_start.tv_usec)/1000000.0;
                    total_ms_time = total_ms_time + duration_ms;

                    ///////////////////Checking ////////////////////////////////
                    int y = 0;
                    for (int err = 5; err <= error_max; err = err + error_step)
                    {
                        k = err;
                        y++;
                        //cout << "------LET-----" << endl;
                        gettimeofday(&checking_le_start,NULL);
                        stree.changlawler_let(fasta_seq_len, read_len, k);
                        gettimeofday(&checking_le_finish,NULL);
                        double duration_checking_let=checking_le_finish.tv_sec-checking_le_start.tv_sec+(checking_le_finish.tv_usec-checking_le_start.tv_usec)/1000000.0;
                        running_time_LET[y-1] = running_time_LET[y-1] + duration_checking_let;
                        
                        //cout << "------SET-----" << endl;
                        if (k <= 20)
                        {
                            gettimeofday(&checking_set_start,NULL);
                            stree.changlawler_set(fasta_seq_len, read_len, k);
                            gettimeofday(&checking_set_finish,NULL);
                            double duration_checking_set=checking_set_finish.tv_sec-checking_set_start.tv_sec+(checking_set_finish.tv_usec-checking_set_start.tv_usec)/1000000.0;
                            running_time_SET[y-1] = running_time_SET[y-1] + duration_checking_set;
                        }
                    }
                    stree.free_memory_usage();
                    if (count ==  100)
                        break;

                }
                //cout << "Species: " << fasta_ref[num_fasta] <<endl;
                logile << "Species: " << fasta_ref[num_fasta] <<endl;
                //cout << "CL.LET Result" << endl;
                logile << "CL.LET Result" << endl;
                for (int i = 0; i< running_time_LET.size();i++)
                {
                   // cout << "k = " << i + 1 << ", running time = " << running_time_LET[i] << ", matching statistics time: " << total_ms_time << endl;
                   if (running_time_LET[i] != 0)
                        logile << "k = " << i + 1 << ", checking LET time: " << running_time_LET[i] << ", matching statistics time: " << total_ms_time << endl;
                }
                //cout << "CL.SET Result " << endl;
                logile << "CL.SET Result" << endl;
                for (int i = 0; i< running_time_SET.size();i++)
                {
                    //cout << "k = " << i + 1 << ", running time = " << running_time_SET[i] << ", matching statistics time: " << total_ms_time << endl;
                    if (running_time_SET[i] != 0)
                        logile << "k = " << i + 1 << ", checking SET time: " << running_time_SET[i] << ", matching statistics time: " << total_ms_time << endl;
                }
                kseq_destroy(read);
                gzclose(fp_fq);
            }
            */
            
        }
        fasta_seq.clear();
        fasta_seq.shrink_to_fit();
    }



    logile.close();

    


    return 0;

}
