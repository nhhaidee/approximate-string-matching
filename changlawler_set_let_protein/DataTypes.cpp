#include <string>
#include <vector>
#include <iostream>
#include "DataTypes.h"

using namespace std;


void DNA_Translate(string *orig_str)
{
    for(string::iterator it=orig_str->begin();it<orig_str->end();it++)
    { 
        
        if(*it=='N' || *it=='n')
        {
            *it='Z'; // Not use N, so change to character N to Z after T
        }     
        if(*it=='a')
        {
            *it='A';
        }
        if(*it=='c')
        {
            *it='C';
        }
        if(*it=='g')
        {
            *it='G';
        }
        if(*it=='t')
        {
            *it='T';
        }
        if(*it=='W' || *it=='w')
        {
            *it='T';
        }
        if(*it=='S' || *it=='s')
        {
            *it='G';
        }
        if(*it=='M' || *it=='m')
        {
            *it='C';
        }
        if(*it=='K' || *it=='k')
        {
            *it='T';
        }
        if(*it=='R' || *it=='r')
        {
            *it='G';
        }
        if(*it=='Y' || *it=='y')
        {
            *it='C';
        }
        if(*it=='B' || *it=='b')
        {
            *it='G';
        }
        if(*it=='D' || *it=='d')
        {
            *it='T';
        }
        if(*it=='H' || *it=='h')
        {
            *it='C';
        }
        if(*it=='V' || *it=='v')
        {
            *it='G';
        }
    }
}
