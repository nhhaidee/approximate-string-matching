///Author:Hai Nguyen
///Created on Oct 2018


#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sys/time.h>
#include <vector>
#include <array>
#include <string>
#include <tr1/unordered_map>
#include <algorithm>
#include <zlib.h>
#include "kseq.h"
#include "bwt.h"
#include "utils.h"
#include "EDTree.h"
#include "DataTypes.h"
#include <math.h>    


KSEQ_INIT(gzFile, gzread)

struct Text_intervals
{
    int64_t start_point;
    int64_t end_point;
};

bool compareInterval(Text_intervals l1, Text_intervals l2)
{
    return (l1.start_point < l2.start_point);
}

void dp (string P, string& T, fmint_t k, fmint_t start, fmint_t end){
	vector<int> H;
	H.clear();
	H.shrink_to_fit();
	int top = k + 1;
	int C, E;
    fmint_t pos;
    fmint_t j;
	for (int i = 0; i <= P.length(); i++) {
		H.push_back(i);
	}
	for (j = start; j <= end; j++) { // For each column
		C = 0;
		if (T[j] == '%')
		{
			H.clear();
			H.shrink_to_fit();
			top = k + 1;
			for (int i = 0; i <= P.length(); i++) 
			{
				H.push_back(i);
			}
		}
		for (int i = 1; i <= top; i++) { // Scan from 1 to k
			if (P[i-1] == T[j])
				E = C;
			else
				E = min(min(H[i - 1] + 1, H[i] + 1), C + 1);
			C = H[i];
			H[i] = E;
		}
        
		while (H[top] > k)
			top = top - 1;
		if (top == P.length())
			cout << "Position: " << j << " k: " << H[P.length()] << endl;
		else
			top = top + 1;
	}

}

using namespace std;

//int main(int argc,char *argv[])
int main()
{
    struct timeval start,finish;    
    struct timeval start_phase1,finish_phase1;
    struct timeval start_phase2,finish_phase2;
    int l, count=0;
    double duration;


    string fasta_file="proteinsequence30M.fa";
    string fastq_file="proteinreads_300bps.fq";
    string out_path="./out.txt";
    cout <<" Species " << fasta_file << endl;
    cout <<" Species (Fastq) " << fastq_file << endl;
    
    fmint_t fasta_length;
    ChromIndex chrindex;
    string fasta;
    string fasta_seq, fasta_seq_orig;
    FMIndex fmindex;
    {
        gzFile fp_fa;
        kseq_t *seq;
        fp_fa = gzopen(fasta_file.c_str(), "r");
        seq = kseq_init(fp_fa);   
        while ((l = kseq_read(seq)) >= 0)
        {
            FastaHead head(seq->name.s,seq->seq.l);
            chrindex.addhead(head);
            fasta_seq.append(seq->seq.s);
            fasta_seq.push_back('%');
        }
        kseq_destroy(seq);
        gzclose(fp_fa);
        chrindex.adjust();
        fasta_length = fasta_seq.length();
        //DNA_Translate(&fasta_seq);
        fasta_seq_orig = fasta_seq;

        gettimeofday(&start, NULL);
        reverse(fasta_seq.begin(),fasta_seq.end());

        fmindex.transform(&fasta_seq);//pass pointer of s and transform to BWT String
    }
    cout << "Starting build FM index..." << endl;
    fmindex.buildIndex(); // Build FM-Index for Searching
    cout << "Done" << endl;
    gettimeofday(&finish, NULL);
    duration=finish.tv_sec-start.tv_sec+(finish.tv_usec-start.tv_usec)/1000000.0;
    cout<<"BWT construction time: "<<duration<<" s"<<endl;


//    reverse(fasta_seq.begin(),fasta_seq.end());
   
    fmint_t bwt_str_size=0,bwt_str_capacity=0,sa_size=0,sa_capacity=0,occ_size=0,C_size=0,C_capacity=0;
    bwt_str_size=sizeof(char)*fmindex.returnBWTString().size();
    bwt_str_capacity=sizeof(char)*fmindex.returnBWTString().capacity();
    sa_size=sizeof(fmint_t)*fmindex.returnSA().size();
    sa_capacity=sizeof(fmint_t)*fmindex.returnSA().capacity();
    occ_size=sizeof(tr1::unordered_map<char,fmint_t>::value_type)*fmindex.returnOcc().size();
    //cout << "sizeof(tr1::unordered_map<char,fmint_t>::value_type) " << sizeof(tr1::unordered_map<char,fmint_t>::value_type) << endl;
    for(fmint_t i=0;i<(fmint_t)fmindex.returnCheckpoints().size();i++)
    {
        //cout << "fmindex.returnCheckpoints()[i].size(): " << fmindex.returnCheckpoints()[i].size() << endl;
        C_size+=sizeof(tr1::unordered_map<char,fmint_t>::value_type)*fmindex.returnCheckpoints()[i].size();
    }



    cout<<"bwt_str_size: "<<(bwt_str_size)<<" bytes"<<endl;
    cout<<"sa_size: "<<(sa_size)<<" bytes"<<endl;
    cout<<"occ_size: "<<(occ_size)<<" bytes"<<endl;
    cout<<"C_size: "<<(C_size)<<" bytes"<<endl;




    FMIndex *fmidx;
    fmidx = &fmindex;
    fmint_t k,sub = 10;
    vector<double>running_time;
    vector<double>running_time_foreach_k_phase1;
    vector<double>running_time_foreach_k_phase2;
    vector<fmint_t> ncheckingpoints_foreach_k;
    vector<int> ncheckingregion_length_foreach_k;
    vector<double>running_time_phase1;
    vector<double>running_time_phase2;
    vector<fmint_t> ncheckingpoints;
    vector<int> ncheckingregion_length;

    
    int no_reads;
    cout << "Number of reads = ";
	cin >> no_reads;
    cout << "Number of subpatterns l = " << sub << endl;
    
    
    vector<fmint_t> checkpoints;
    vector<Text_intervals> sub_intevals;
    vector<fmint_t> total_checking_points;
    for (fmint_t err = 5; err <=35; err += 5)
    {
        gzFile fp_fq;
        kseq_t *read;
        fp_fq = gzopen(fastq_file.c_str(), "r");
        read = kseq_init(fp_fq);
        count = 0;
        k = err;
        gettimeofday(&start,NULL);
        fmint_t check = 0;
        EditDistanceTree edistTree;
        // clear running_time_phase1 vector for each values of k
        running_time_phase1.clear();
        running_time_phase2.clear();
        ncheckingpoints.clear();
        ncheckingregion_length.clear();

        while ((l = kseq_read(read)) >= 0)
        {
            count++;
            cout << "Read Sequence: " << read->seq.s << endl;
            cout << "Read Sequence Length: " << read->seq.l << ", Read #" << count << endl;
            string q(read->seq.s);    
            //cout << "Read Sequence: " << q << endl;  
            int a = 0;
            int b;
            int r;
            int subpatterns_len;
            int errors;
            k = err;
            sub_intevals.clear();
            /////////////////// Phase 1 ////////////////////////////////
            gettimeofday(&start_phase1,NULL);

            for (r = 0; r < sub; r++)
            {
                checkpoints.clear();
                subpatterns_len =(r < q.length()%sub) ? (q.length()/sub+1) : q.length()/sub;
                b = a + subpatterns_len - 1;
                string sub_pattern = q.substr(a,subpatterns_len);
                //cout << a << " " << b << " " << k/sub << " " << q.substr(a,subpatterns_len) << endl;
                checkpoints = edistTree.scanning(fmidx, fasta_length, sub_pattern, k/sub);
                if (checkpoints.size() >0)
                {
                    
                    for (int i = 0; i <checkpoints.size(); i++)
                    {
                        //cout << "Checkpoint= " << checkpoints[i] << endl;
                        Text_intervals l;
                        //cout << "Checkpoint " << checkpoints[i] << endl;
                        l.start_point = checkpoints[i] - b + 1 -k;
                        //cout << "l.start_point " << l.start_point << endl;
                        if (l.start_point < 0)
                            l.start_point = 0;
                        l.end_point = checkpoints[i] - b + 1 + q.length() + k;
                        if (l.end_point >= fasta_length -2)
                            l.end_point = fasta_length -2;
                        //cout << l.start_point << " - " << l.end_point << endl;
                        sub_intevals.push_back(l);
                    }
                }

                a = b + 1;
            }

            gettimeofday(&finish_phase1,NULL);
            double duration_phase1=finish_phase1.tv_sec-start_phase1.tv_sec+(finish_phase1.tv_usec-start_phase1.tv_usec)/1000000.0;
            running_time_phase1.push_back(duration_phase1);
             /////////////////// End of Phase 1 ////////////////////////////////
            // Sort intervals of checkpoints
            cout << "Total Checking Points sub_intevals: " << sub_intevals.size() << endl;
            //check = check + sub_intevals.size();

             /////////////////// Phase 2 ////////////////////////////////
            ::gettimeofday(&start_phase2,NULL);
            if (sub_intevals.size() > 0)
            {
                sort(sub_intevals.begin(), sub_intevals.end(), compareInterval);
                stack<Text_intervals> s;
                s.push(sub_intevals[0]);
                for (fmint_t i = 1 ; i < sub_intevals.size(); i++)
                {
                    Text_intervals top = s.top();
                    if (top.end_point < sub_intevals[i].start_point)
                        s.push(sub_intevals[i]);
                    else if (top.end_point < sub_intevals[i].end_point)
                    {
                        top.end_point = sub_intevals[i].end_point;
                        s.pop();
                        s.push(top);
                    }
                }
                //cout << "Out put result: " << endl;;
                int ncheckpoints = 0;
                int regionlength = 0;
                while (!s.empty())
                {
                    Text_intervals t = s.top();
                    ncheckpoints = ncheckpoints + 1;
                    regionlength = regionlength + abs(t.end_point - t.start_point + 1);
                    dp(q, fasta_seq_orig, k, t.start_point, t.end_point);
                    s.pop();
                }
                ncheckingpoints.push_back(ncheckpoints);
                ncheckingregion_length.push_back(regionlength);
                cout << endl;
                cout << "Total checking points: " << ncheckpoints << endl;
            }
            else
            {
                ncheckingpoints.push_back(0);
                ncheckingregion_length.push_back(0);
            }
            gettimeofday(&finish_phase2,NULL);
            double duration_phase2=finish_phase2.tv_sec-start_phase2.tv_sec+(finish_phase2.tv_usec-start_phase2.tv_usec)/1000000.0;
            running_time_phase2.push_back(duration_phase2);
              /////////////////// End of Phase 2 ////////////////////////////////
            if (count == no_reads)
                break;
        }


        gettimeofday(&finish,NULL);
        duration=finish.tv_sec-start.tv_sec+(finish.tv_usec-start.tv_usec)/1000000.0;
        total_checking_points.push_back(check);
        running_time.push_back(duration);
        cout<<"EBWT searching time: "<<duration<<" s"<<endl;
        cout<<"read amount: "<<count<<endl;

        double total_time_phase1 = 0;
        double total_time_phase2 = 0;
        fmint_t final_checkpoints = 0;
        int final_length_regions = 0;
        for (int j = 0; j < no_reads; j++)
        {
            total_time_phase1 = total_time_phase1 + running_time_phase1[j];
            total_time_phase2 = total_time_phase2 + running_time_phase2[j];
            final_checkpoints = final_checkpoints + ncheckingpoints[j];
            final_length_regions = final_length_regions + ncheckingregion_length[j];
        }
        fmint_t final_checkpoints1 = ceil((double)final_checkpoints / (double)no_reads);
        running_time_foreach_k_phase1.push_back(total_time_phase1);
        running_time_foreach_k_phase2.push_back(total_time_phase2);
        ncheckingpoints_foreach_k.push_back(final_checkpoints1);
        ncheckingregion_length_foreach_k.push_back(final_length_regions/final_checkpoints);
        kseq_destroy(read);
        gzclose(fp_fq);
    }

    int error_out = 0;
    for (int i =0; i <running_time.size(); i++)
    {
        error_out+=5;
        cout << "k_errors = " << error_out << ", running time = " << running_time[i] << ", running time phase1 =" <<  running_time_foreach_k_phase1[i] << ", running time phase2 =" <<  running_time_foreach_k_phase2[i] << ", avg checking points =" <<  ncheckingpoints_foreach_k[i] << ", avg checking region length =" <<  ncheckingregion_length_foreach_k[i]<< endl;
    }
   
    return 0;
}

