#include <vector>
#include <tr1/unordered_map>
#include <algorithm>
#include <string>
#include <iostream>
#include <stack>
#include <stdio.h>
#include <thread>
#include <chrono>
#include "divsufsort.h"
#include "DataTypes.h"
#include "bwt.h"

using namespace std;

/*----------------------------------------------------------
* Build bwt and fmindex
* ---------------------------------------------------------*/
void FMIndex::transform(string *orig_str)
{
    orig_str->shrink_to_fit();
    size_t found=orig_str->find(EOS);
    if(found!=string::npos)
    {
        cout<<"original string contains $"<<endl;
        exit (EXIT_FAILURE);
    }
    //DNA_Translate(orig_str);
    orig_str->push_back(EOS);
    len=orig_str->size();
    cout << "len " << len << endl;
    full_sa=new saint64_t[len];
    cout << "Rotating all suffixes and Getting SA..."<< endl;
    divsufsort((unsigned char*)orig_str->c_str(),full_sa,len);
    cout << "Done" << endl;
    encoded_bwt_str.reserve(len);
    //sleep(30);
    cout << "Starting calculate BWT String ..." << endl;
    for(fmint_t i=0;i<len;i++)
    {        
        //cout <<  i+ 1 << "   " << full_sa[i] << "   " << orig_str->at(full_sa[i]) << " ";
        switch(full_sa[i])
        {
            case 0: encoded_bwt_str.push_back(encode(EOS)); 
                   // cout << "   " << EOS << "   " << full_sa[i] + len - 1<< endl; 
                    break;
            default: encoded_bwt_str.push_back(encode(orig_str->at(full_sa[i]-1)));
                    //cout << "   " << orig_str->at(full_sa[i]-1) << "   " << full_sa[i]-1 << endl; 
                     break;
        }

    }
    cout << "Done" << endl;
    encoded_bwt_str.shrink_to_fit();
}

void FMIndex::buildIndex()
{
    cout << "Getting SA sample"<< endl;
    _calcCheckpointingSA(full_sa);
    cout << "Getting SA LF Checkpoints"<< endl;
    _calcOccAndCheckpoints(encoded_bwt_str);
    cout << "Done"<< endl;
}
/*----------------------------------------------------------
* For search using FMindex directly
* ---------------------------------------------------------*/

vector<fmint_t>& FMIndex::search(vector<u_int8_t> &q)
{
    fmint_t top=0,bot=len;
    _findBound(q,top,bot);//common process should use _findBound()
    // find the location of the suffixes
    // by walking the reverse text from that position
    // with lf mapping
    matches.clear();
    if (top < bot)
    {
        for(fmint_t i=top;i<bot;i++)
        {
            fmint_t pos=_walk(i);
            matches.push_back(pos);
        }
    }
    else
        matches.clear();
    //sort(matches.begin(),matches.end());
    return matches;
}
/*----------------------------------------------------------
* Search bound for each nucleotide, using for ED Tree
* ---------------------------------------------------------*/
vector<fmint_t> FMIndex::_searchBound(char& q,fmint_t& top,fmint_t& bot)
{
    fmint_t _top,_bot;
    range.clear();
    _top=_lf(top,encode(q));
    _bot=_lf(bot,encode(q));
    if(_top==_bot){
        range.clear();
    }   
    else {
        range.push_back(_top);
        range.push_back(_bot);
    } 
    return range;  
}

/*----------------------------------------------------------
* For query search
* ---------------------------------------------------------*/


///find the first and last suffix positions for query q
void FMIndex::_findBound(vector<u_int8_t> &q,fmint_t &top,fmint_t &bot)
{
    fmint_t new_top,new_bot;
    vector<u_int8_t>::iterator qc;
    for(qc=q.begin();qc!=q.end();++qc)
    {
        new_top=_lf(top,*qc);
        new_bot=_lf(bot,*qc);
        cout <<  decode(*qc) << " " << new_top << " " << new_bot << endl;
        if(new_top<new_bot)
        {   //Found
            top=new_top;
            bot=new_bot; 
        }
        else
        {
            //qc=qc-1;
            top=new_top;
            bot=new_bot;
            break; // Not found
        }
    }    
}


fmint_t FMIndex::walk(fmint_t idx)
{
    //walk to the a checkpoint using lf mapping
    fmint_t r=0,i=idx;
    //bool quit_early=false;
    while(i%gap)
    {
        r=r+1;
        i=_lf(i,encoded_bwt_str[i]);
    }
    //usage of sa for faster searches
    //if(!quit_early)
    //r=(sa[i/gap]+r)%len;
    return (sa[i/gap]+r)%len;
}

void FMIndex::_calcCheckpointingSA(saint64_t *full_sa)
{
    sa.reserve(len/gap+1); //get ceiling of len/gap
    for(size_t i=0;i<len;i+=gap)
        sa.push_back((fmint_t)full_sa[i]);
    delete [] full_sa;
    full_sa = NULL;
}

void FMIndex::_calcOccAndCheckpoints(vector<u_int8_t> &encoded_s)
{
    /*
    tr1::unordered_map<char,fmint_t> A;//letter count, key is character (A, C, G, T, $), value is the number of appearances of character within L[1..k]
    fmint_t i=0;
    for(string::iterator c=s.begin();c!=s.end();++c,i++)
    {
            //if (*c != '$' and *c != '%'){
        if(i%step==0)
        {
            C.push_back(A);  
        }  
        if(A.find(*c)==A.end()) //dont find c in A
        {
            A[*c]=1;
        }
        else // find c in A
        {
            A[*c]=A[*c]+1;
        }
    }
    C.shrink_to_fit();
    vector<char> letters;
    for(tr1::unordered_map<char,fmint_t>::iterator it_A=A.begin();it_A!=A.end();++it_A)
    {
        letters.push_back(it_A->first);
    }
    sort(letters.begin(),letters.end());//sort letters, if not sorted by map/unorded_map
    fmint_t idx=0;
    for(vector<char>::iterator c=letters.begin();c!=letters.end();++c)
    {
        occ[*c]=idx;//first index of letters
        idx=idx+A[*c];
    }

    C_len=C.size();
    */
    array<fmint_t,CHILDREN_SIZE+3> A;
    A.fill(0);
    fmint_t i;
    C.reserve(len/step+1);
    for(i=0;i<len;i++)
    {
        if(i%step==0)
            C.push_back(A);
        A[encoded_s[i]]++;
        //cout << decode(encoded_s[i])  << " " << A[encoded_s[i]] << endl;
    }
    //occ.resize(CHILDNUM+2);
    fmint_t idx=0;

    occ[CHILDREN_SIZE+1]=idx;
    idx+=A[CHILDREN_SIZE+1];

    occ[CHILDREN_SIZE+2]=idx;
    idx+=A[CHILDREN_SIZE+2];

    for(i=0;i<CHILDREN_SIZE+1;i++)
    {
        occ[i]=idx;//first index of letters
        idx+=A[i];
    }
    C_len=C.size();
    //cout << "C_len " << C_len << endl;
}




///Count the number of a letter upto idx in s using checkpoints
///
/// Arguments:
/// idx     -- count upto this position
/// letter  -- count for this letter
/// C       -- is the list of checkpoints
/// step    -- is the step of the checkpoints
/// bwt_str -- the transformed string
fmint_t FMIndex::_countLetterWithCheckpoints(const fmint_t idx,const u_int8_t letter)
{
    fmint_t check=(idx+(step/2))/step;
    if(check>=C_len)
    {
        check=C_len-1;
    }
    fmint_t pos=check*step;

    //count of the letter s[idx] upto pos (not included)
    fmint_t count;/*
    tr1::unordered_map<u_int8_t,fmint_t>::iterator Ccheck_it= C[check].find(letter);
    if(Ccheck_it==C[check].end())//cannot find
    {
        count=0;
    }
    else
    {
        count=Ccheck_it->second;
    }*/
    count=C[check][letter];
    //range between pos and idx
    fmint_t range[2];
    if(pos<idx)
    {
        range[0]=pos;
        range[1]=idx;
    }
    else
    {
        range[0]=idx;
        range[1]=pos;
    }

    //count of letters between pos and idx
    fmint_t i,k=0;
    for(i=range[0];i<range[1];i++)
    {
        if(letter==encoded_bwt_str[i])
        {
            k=k+1;
        }
    }

    //calculate the letter count upto idx (not included)
    if(pos<idx)
    {
        count=count+k;
    }
    else
    {
        count=count-k;
    }

    return count;
}

fmint_t FMIndex::_walk(const fmint_t idx)
{
    //walk to the a checkpoint using lf mapping
    fmint_t r=0,i=idx;
    //bool quit_early=false;
    while(i%gap)
    {
        r=r+1;
        i=_lf(i,encoded_bwt_str[i]);
    }
    //usage of sa for faster searches
    //if(!quit_early)
    //r=(sa[i/gap]+r)%len;
    return (sa[i/gap]+r)%len;
}
/*
fmint_t FMIndex::_occ(char qc)
{
    fmint_t c;
    tr1::unordered_map<char,fmint_t>::iterator occ_it=occ.find(qc);
    if(occ_it==occ.end())//cannot find qc
        c=0;
    else
        c=occ_it->second;
    return c;
}
*/
fmint_t FMIndex::_count(fmint_t idx,char qc)
{
    fmint_t count=_countLetterWithCheckpoints(idx,qc);
    return count;
}
    ///get the nearest lf mapping for letter qc at position idx
fmint_t FMIndex::_lf(const fmint_t idx,const u_int8_t qc)
{
    fmint_t o,c;
    o= occ[qc];
    c=_count(idx,qc);
    return o+c;
    //return occ[qc]+_countLetterWithCheckpoints(idx,qc);
}