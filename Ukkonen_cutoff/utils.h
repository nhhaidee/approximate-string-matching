#ifndef UTIL_H
#define UTIL_H

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sys/time.h>
#include <vector>
#include <array>
#include <string>
#include <tr1/unordered_map>

using namespace std;

typedef int32_t saint64_t;
typedef u_int32_t fmint_t;

#define EOC '%'
#define BIT_SET(a,b) ((a) |= (1<<(b)))
#define BIT_CLEAR(a,b) ((a) &= ~(1<<(b)))
#define BIT_FLIP(a,b) ((a) ^= (1<<(b)))
#define BIT_CHECK(a,b) ((a) & (1<<(b)))


u_int8_t encode(char &c)
{
    u_int8_t i;
    switch(c)
    {
        case 'a':
        case 'A': i=0;break;
        case 'c':
        case 'C': i=1;break;
        case 'g':
        case 'G': i=2;break;
        case 't':
        case 'T': i=4;break;
        default: i=3;break;
    }
    return i;
}

char decode(size_t i)
{
    char c;
    switch(i)
    {
        case 0: c='A';break;
        case 1: c='C';break;
        case 2: c='G';break;
        case 4: c='T';break;
        default: c='N';break;
    }
    return c;
}

class FastaHead
{
public:
    string name;
    fmint_t length;
    fmint_t start; // if fasta is reversed then (start,end],
    fmint_t end;   // else, [start,end)

    FastaHead(string s,fmint_t l)
    {
        name=s;
        length=l;
        start=-1;
        end=-1;
    }

};

class RealPosition
{
public:
    string name;
    fmint_t pos;
};

class ChromIndex
{
public:
    ChromIndex()
    {
        READY=false;
    }

    void addhead(FastaHead head)
    {
        heads.push_back(head);
    }

    void adjust()
    {
        fmint_t tmp_start=0;
        for(size_t i=heads.size()-1;i>0;i--)
        {
            heads[i].start=tmp_start;
            heads[i].end=tmp_start+heads[i].length;
            tmp_start=heads[i].end+1;
        }
        heads[0].start=tmp_start;
        heads[0].end=tmp_start+heads[0].length;
        READY=true;
    }

    RealPosition calRealPos(fmint_t &p,size_t l)
    {
        if(!READY)
            adjust();
        for(size_t i=0;i<heads.size();i++)
        {
            if(p>heads[i].start)
            {
                if(p<=heads[i].end)
                {
                    rpos.pos=heads[i].length-p+heads[i].start-l+2;
                    rpos.name=heads[i].name;
                }
                else
                {
                    cout<<"read wrong, because it matches to 'end marker'"<<endl;
                    rpos.pos=p;
                    rpos.name="WrongRead";
                }
                break;
            }
        }
        return rpos;
    }

private:


    vector<FastaHead> heads;
    RealPosition rpos;
    bool READY;

};





#endif // UTIL_H
