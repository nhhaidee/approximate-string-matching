#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sys/time.h>
#include <vector>
#include <array>
#include <string>
#include <tr1/unordered_map>
#include <zlib.h>
#include "kseq.h"
#include "utils.h"



KSEQ_INIT(gzFile, gzread)

using namespace std;


void dp (string& P, string& T, fmint_t k){
	    	//int *H;
	vector<int> H;
	//H = new int[75 + 1];
	H.clear();
	H.shrink_to_fit();
	fmint_t seq_len = T.length();
	int top = k + 1;
	int C, E;
	for (int i = 0; i <= P.length(); i++) {
		H.push_back(i);
	}
	for (fmint_t j = 1; j <= seq_len; j++) {
		C = 0;

		
		if (T[j-1] == '%')
		{
			H.clear();
			H.shrink_to_fit();
			top = k + 1;
			for (int i = 0; i <= P.length(); i++) 
			{
				H.push_back(i);
			}
		}
		
		for (int i = 1; i <= top; i++) { 
			if (P[i-1] == T[j-1])
				E = C;
			else
				E = min(min(H[i - 1] + 1, H[i] + 1), C + 1);
			C = H[i];
			H[i] = E;
		}
		while (H[top] > k)
			top = top - 1;
		if (top == P.length())
			cout << "Position: " << j -1 << " k: " << H[P.length()] << endl;
		else
			top = top + 1;
	}
	//delete[] H;
	//H = NULL;
}

int main() {

	struct timeval start,finish;
	struct timezone tz;
	double duration;
 
    string fasta_file="Cyanidioschyzon_merolae.ASM9120v1.dna.chromosome.1.fa";
    string fastq_file="Cyanidioschyzon_merolae_reads_300bps.fq";
    fmint_t fasta_length;
	int l, count=0;
	string T; // Fasta Sequence
 
        gzFile fp_fa;
        kseq_t *seq;
        fp_fa = gzopen(fasta_file.c_str(), "r");
        seq = kseq_init(fp_fa); 
        while ((l = kseq_read(seq)) >= 0)
        {
            //FastaHead head(seq->name.s,seq->seq.l);

            T.append(seq->seq.s);
			T.push_back('%');
            //fasta_seq.push_back(EOC);
        }
        kseq_destroy(seq);
        gzclose(fp_fa);
        fasta_length = T.length();
        cout<<"Fasta length: "<<T.length()<<endl;



    int *H;
	fmint_t k;
    gzFile fp_fq;
    kseq_t *read;
    //fp_fq = gzopen(fastq_file.c_str(), "r");
   // read = kseq_init(fp_fq);
    //cout << "Enter k errors = ";
	//cin >> k;
    vector<double>running_time;
	string pattern ="GCGGTACCAAAATCCTCGCCTTCAAACCGAAACCCCCGGTGCACCTCGGCGATAAGGCTGCTCATGCGGCTATCGCTGTGGTCTATACGCAGAACCGCCTCGGTGCAGCTTGTAGACGACACCTCCATCGACATATCCCGAGTGCGCCGGAGCGAATCCTCGATGCTCCCGAGATGGTTGATGACTACTATCTGAACCTGTTGGACTGGAGTGCCAACAACGTGCTGGCGGTGGCGCTCGGTTCGGCGGTTTACCTCTGGAATGCCAGCACTGGTGGCATTGAGCAGCTCACTGACCTTG";
	cout << "Pattern: " << pattern;
	cout << endl;
	for (int err  = 5; err <= 35; err = err +5){
		//fp_fq = gzopen(fastq_file.c_str(), "r");
   		//read = kseq_init(fp_fq);
		gettimeofday(&start, NULL);
		k = err;
		cout << "k difference: " << k << endl;
		/*
		while ((l = kseq_read(read)) >= 0)
		{
			count++;
			cout << "Read Sequence: " << read->seq.s << endl;
			cout << "Read Sequence Length: " << read->seq.l << endl;
			string q(read->seq.s);
			dp(q, T, k);
			if (count == 100)
				break;

		}
		*/
		dp(pattern, T, k);
		gettimeofday(&finish, NULL);
		//kseq_destroy(read);
	    //gzclose(fp_fq);
		duration=finish.tv_sec-start.tv_sec+(finish.tv_usec-start.tv_usec)/1000000.0;
		cout<<"searching time: "<<duration<<" s"<<endl;
		running_time.push_back(duration);
	}
	for (int re = 0 ; re < running_time.size(); re++ )
	{
		cout << "k= " << (re + 1)*5 << ", running time = " << running_time[re] << endl;
	}
	return 0;

}
