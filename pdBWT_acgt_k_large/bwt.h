/*Author:Yujia Wu
Created on Dec 20 2014
Modified and Updated by Hai Nguyen on Jan 11 2018
Can build bwt for a sequence within 2^32-1 ~ 4.3 Billion bp
*/
#ifndef BWT_H
#define BWT_H

#include <vector>
#include <tr1/unordered_map>
#include <string>
#include <iostream>
#include "DataTypes.h"
#include "utils.h"

using namespace std;


class FMIndex
{
public:
    FMIndex(fmint_t STEP=4,fmint_t GAP=16)
    {
        step=STEP>1?STEP:1;
        gap=GAP>1?GAP:1;
        EOS='$'; // END OF THE WHOLE STRING
        EOC='%'; // END OF THE CHROMOSOME
        matches.reserve(500);
    }
    /*----------------------------------------------------------
     * For building bwt and fmindex
     * ---------------------------------------------------------*/

    ///transform string to bwt string
    void transform(string *orig_str);

    ///build the index
    void buildIndex();

    /*----------------------------------------------------------
     * For search using FMindex directly
     * ---------------------------------------------------------*/
    ///search the positions of query q
    vector<fmint_t>& search(vector<u_int8_t> &q);
  
    vector<fmint_t> _searchBound(char& q,fmint_t& top,fmint_t& bot);

    fmint_t walk(fmint_t idx);

    ///return transformed string
    vector<u_int8_t>& returnBWTString()
    {
        return encoded_bwt_str;
    }

    array<fmint_t,CHILDREN_SIZE+3>& returnOcc()
    {
        return occ;
    }

    vector< array<fmint_t,CHILDREN_SIZE+3> >& returnCheckpoints()
    {
        return C;
    }

    vector<fmint_t>& returnSA()
    {
        return sa;
    }

    size_t returnBWTStringLength()
    {
        return len;
    }

    size_t returnCheckpointsLength()
    {
        return C_len;
    }

private:

    fmint_t step;//step of the checkpoints C
    fmint_t gap;//gap of the checkpoints sa
    char EOS;//end mark for whole string
    char EOC;//end mark for each chromosome substring
    //string bwt_str;//transformed bwt string
    vector<u_int8_t> encoded_bwt_str;
    saint64_t *full_sa;//suffix array
    vector<fmint_t> sa;//checkpoints of suffix array
    array<fmint_t, CHILDREN_SIZE + 3> occ;
    //tr1::unordered_map<char,fmint_t> occ;//first occurance of letters
    //vector< tr1::unordered_map<char,fmint_t> > C;//list of checkpoints
    vector< array<fmint_t, CHILDREN_SIZE+3> > C;
    size_t len;//length of transformed bwt string
    size_t C_len;//length of checkpoints
    vector<fmint_t> matches;//query results
    vector<fmint_t> range;

  
    //friend class boost::serialization::access;//serialization

    /*----------------------------------------------------------
     * For building bwt and fmindex
     * ---------------------------------------------------------*/


    ///calculate the first occurance of a letter in sorted string s or say C[c]
    ///count the number of letters for each step and return list of the counts
    /// s is the bwt transformed string
    /// using step parameters

    void _calcOccAndCheckpoints(vector<u_int8_t> &encoded_s);
    void _calcCheckpointingSA(saint64_t *full_sa);

    /*----------------------------------------------------------
     * For query search
     * ---------------------------------------------------------*/

    ///find the first and last suffix positions for query q
    void _findBound(vector<u_int8_t> &q,fmint_t &top,fmint_t &bot);

    ///Count the number of a letter upto idx in s using checkpoints
    fmint_t _countLetterWithCheckpoints(const fmint_t idx,const u_int8_t letter);

    ///find the offset in position idx of transformed string from the beginning
    fmint_t _walk(fmint_t idx);

    ///get the first occurance of letter qc in left-column
    fmint_t _occ(char qc);

    ///count the occurances of letter qc (rank of qc) upto position idx
    fmint_t _count(fmint_t idx,char qc);

    ///get the nearest lf mapping for letter qc at position idx
    fmint_t _lf(const fmint_t idx,const u_int8_t qc);

};



#endif // BWT_H
