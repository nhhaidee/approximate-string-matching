#include <string>
#include <vector>
#include <iostream>
#include <stack>
#include <map>
#include <stdio.h>
#include <algorithm>
#include <tr1/unordered_map>
#include <sys/time.h>
#include "bwt.h"
#include "DataTypes.h"
#include "EDTree.h"
#include "utils.h"

using namespace std;

EditDistanceTree::EditDistanceTree()
{
    /*
    int row = m +1;
    int col = m + k + 2;
    int i, j;
    
    D = new int*[row];
    L = new int*[row];
    for (int i = 0; i < row; i++)
    {
        D[i] = new int[col];
        L[i] = new int[col];
    }
    
    for (i = 0; i < m + k + 2; i++)
    for (j = 0; j < m + 1; j++)
    {
        D[j][i] = j;
        L[j][i] = 0;
    }
    */
}

EditDistanceTree::~EditDistanceTree()
{
    //int row = m +1;
    //int col = m + k + 2;
    /*
    for (int i = 0; i < m + 1; i++)
    {
        delete[] D[i];
        delete[] L[i];
    }
    delete[] L;
    delete[] D;
    L = NULL;
    D = NULL;
    */
}

vector<fmint_t> EditDistanceTree::findbound(FMIndex *fmidx,char &q,fmint_t &top,fmint_t &bot)
{
    vector<fmint_t> bound;
    bound.clear();
    this->fmindex=fmidx;
    bound = fmindex->_searchBound(q,top,bot);
    return bound;
}

vector<fmint_t> EditDistanceTree::ed_walk(FMIndex *fmidx,fmint_t& top,fmint_t& bot)
{

    this->fmindex=fmidx;
    edmatches.clear();
    for(fmint_t i=top;i<bot;i++){
        fmint_t pos=fmindex->walk(i);
        edmatches.push_back(pos);
    }
    return edmatches;
}

void EditDistanceTree::dp(int depth, string& q, char c)
{
    D[0][depth] = 0; // i = depth
    L[0][depth] = 0;
    int cost;
    int j;
    int len = q.length();
    for (j = 1; j < len + 1; j++)
    {
        D[j][depth] = D[j-1][depth] + 1;
        L[j][depth] = L[j-1][depth];
        if (q[j-1] == c)
        {
            cost = 0;
        }
        else
        {
            cost = 1;
        }
        if ((D[j-1][depth-1] + cost) < D[j][depth])
        {
            if (cost == 0)
            {
                D[j][depth] = D[j-1][depth-1];
            }
            else
            {
                D[j][depth] = D[j-1][depth-1] + 1;
            }
            L[j][depth] = L[j-1][depth-1] + 1;
        }
        if (D[j][depth-1] +1 < D[j][depth])
        {
            D[j][depth] = D[j][depth-1] + 1;
            L[j][depth] = L[j][depth-1] + 1;
        }
    }

}

void EditDistanceTree::edp(int depth, string& subpatterns, char c, int k_top, int errors)
{
    D[0][depth] = 0; // i = depth
    L[0][depth] = 0;
    int cost;
    int top = k_top;
    int j;
    if (klevels_index[depth-1] < klevels[depth-1])
    {
        D[klevels[depth-1]][depth - 1] = klevels[depth-1];
    }
    for (j = 1; j <= top; j++)
    {
        D[j][depth] = D[j-1][depth] + 1;
        L[j][depth] = L[j-1][depth];
        if (subpatterns[j-1] == c)
        {
            cost = 0;
        }
        else
        {
            cost = 1;
        }
        if ((D[j-1][depth-1] + cost) < D[j][depth])
        {
            if (cost == 0)
            {
                D[j][depth] = D[j-1][depth-1];
            }
            else
            {
                D[j][depth] = D[j-1][depth-1] + 1;
            }
            L[j][depth] = L[j-1][depth-1] + 1;
        }
        if (D[j][depth-1] +1 < D[j][depth])
        {
            D[j][depth] = D[j][depth-1] + 1;
            L[j][depth] = L[j][depth-1] + 1;
        }
    }
    while(D[top][depth] > errors)
    {
        top = top - 1;
    }
    klevels_index[depth] = top;
    if (top < subpatterns.length())
    {
        top  = top + 1;
    }
    klevels[depth] = top;
}
/*
int EditDistanceTree::emax_index(int depth)
{
    int j;
    int l;
    for (j = klevels_index[depth]; j >= 0; j--)
    {
        if (D[j][depth] <= k)
        {
            l = j;
            break;
        }
    }
    return l;
}


int EditDistanceTree::max_index(int depth)
{
    int j;
    int l = 0;
    for (j = 0; j < m + 1; j++)
    {
        if (D[j][depth] <= k)
        {
            l = j;
        }
    }
    return l;
}
*/
void EditDistanceTree::construct(FMIndex *fmidx, fmint_t fasta_length, string subpatterns, int errors)
{
    this->fmindex = fmidx;
    fmint_t first;
    fmint_t last;
    int viableprefix_len = 0;
    int read_len = subpatterns.length();
    int largest_index = 0;
    int depth;
    int k_errors;
    int k_top;
    char c;
    int m = subpatterns.length();
    cout << "Sub patterns " << subpatterns << endl;
    klevels.fill(errors+1);
    klevels_index.fill(errors+1);
    stack_level = -1;
    boundstack.fill(0);
    fmint_t internal_nodes = 0;
    fmint_t leaf_nodes = 0;
    fmint_t cout_nodes = 0;

    // Initalize matrix;
    int row = m +1;
    int col = m + errors + 2;
    int i, j;
    
    D = new int*[row];
    L = new int*[row];
    for (int i = 0; i < row; i++)
    {
        D[i] = new int[col];
        L[i] = new int[col];
    }
    
    for (i = 0; i < m + errors + 2; i++)
    {
        for (j = 0; j < m + 1; j++)
        {
            D[j][i] = j;
            L[j][i] = 0;
        }
    }


    pushBound('-', 0, fmindex->returnBWTStringLength(), 0); // Root node
    while(stack_level != -1)
    {
        popBound(); // stack_level decrement by 1
        c = decode_ch(boundstack[4*(stack_level + 1) + 0]); 
        first = boundstack[4*(stack_level + 1) + 1];
        last = boundstack[4*(stack_level + 1) + 2];
        depth = boundstack[4*(stack_level + 1) + 3];
        //cout << c << " " << first << " " << last << " " << depth << endl;
        // Apply Dynamic Programming
        
        if (depth > 0) // dist_level = 0 which is root
        {
            k_top = klevels[depth -1];
            //cout << c << " " << first << " " << last << endl;
            edp(depth, subpatterns, c, k_top, errors);
            largest_index = klevels_index[depth];
            //dp(depth, q, c);
            //largest_index = max_index(depth);
            viableprefix_len = L[largest_index][depth];
            
            if (largest_index == read_len)
            {
                results.clear();
                results=ed_walk(fmidx, first, last);
                k_errors = D[largest_index][depth];
                for(auto r_it=results.begin();r_it!=results.end();++r_it)
                {
                    cout<< "Pos_Rev: "<< *r_it \
                        << ", Ending Pos: " << (fasta_length - *r_it -1) \
                        << ", k: " << k_errors \
                        << ", Substring length: " << viableprefix_len << endl;
                       // << ", subtr from root: " << T.substr((fasta_length - *r_it -depth),depth) << endl;                 
                }
            }
            
        }
    
        if (viableprefix_len == depth) // depth = 0 which is root node
        {
            for(map<u_int8_t,char>::iterator it=Nucleotide.begin(); it!=Nucleotide.end(); ++it)
            {
                edsearches.clear();
                edsearches = findbound(fmidx, it->second, first, last);
                if (edsearches.size() != 0)
                {
                    pushBound(it->second, edsearches[0], edsearches[1], depth + 1);
                    internal_nodes +=1;
                    if (stack_level >= 250)
                    {
                        cout << "Out of stack" << endl;
                    }
                }
            } 
        }
        else
        {
            leaf_nodes += 1;
            //cout << viableprefix_len << " " << depth << endl;
            //cout << "viableprefix_len " << viableprefix_len << ", depth " << depth << endl;
            /*
            results.clear();
            results=ed_walk(fmidx, first, last);
            k_errors = D[largest_index][depth];
            //for(auto r_it=results.begin();r_it!=results.end();++r_it)
            //{
              //  cout<< "Pos_Rev: "<< *r_it 
                   cout << "Ending Pos: " << (fasta_length - results[0] -1) \
                        << ", k: " << k_errors \
                        << ", viable prefix length: " << viableprefix_len \
                        << ", viable prefix: "<<  T.substr((fasta_length - results[0] -viableprefix_len),viableprefix_len)\
                        << ", depth: " << depth \
                        << ", subtr from root: " << T.substr((fasta_length - results[0] -depth),depth) << endl;                       
            //}
            */
            
        }
    }
    for (int i = 0; i < m + 1; i++)
    {
        delete[] D[i];
        delete[] L[i];
    }
    delete[] L;
    delete[] D;
    L = NULL;
    D = NULL;
    cout << "Internal nodes: " << internal_nodes << endl;
    cout << "Leaf nodes: " << leaf_nodes << endl;
}
