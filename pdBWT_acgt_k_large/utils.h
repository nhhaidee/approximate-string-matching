#ifndef UTIL_H
#define UTIL_H
#include "DataTypes.h"
#include <string>
#include <vector>
#include <iostream>

using namespace std;


#define BIT_SET(a,b) ((a) |= (1<<(b)))
#define BIT_CLEAR(a,b) ((a) &= ~(1<<(b)))
#define BIT_FLIP(a,b) ((a) ^= (1<<(b)))
#define BIT_CHECK(a,b) ((a) & (1<<(b)))

u_int8_t encode(char &c);
char decode(size_t i);
void DNA_Translate(string *orig_str);
class FastaHead
{
public:
    string name;
    fmint_t length;
    fmint_t start; // if fasta is reversed then (start,end],
    fmint_t end;   // else, [start,end)

    FastaHead(string s,fmint_t l)
    {
        name=s;
        length=l;
        start=-1;
        end=-1;
    }

};

class RealPosition
{
public:
    string name;
    fmint_t pos;
};

class ChromIndex
{
public:
    ChromIndex()
    {
        READY=false;
    }

    void addhead(FastaHead head)
    {
        heads.push_back(head);
    }

    void adjust()
    {
        fmint_t tmp_start=0;
        for(size_t i=heads.size()-1;i>0;i--)
        {
            heads[i].start=tmp_start;
            heads[i].end=tmp_start+heads[i].length;
            tmp_start=heads[i].end+1;
        }
        heads[0].start=tmp_start;
        heads[0].end=tmp_start+heads[0].length;
        READY=true;
    }

    RealPosition calRealPos(fmint_t &p,size_t l)
    {
        if(!READY)
            adjust();
        for(size_t i=0;i<heads.size();i++)
        {
            if(p>heads[i].start)
            {
                if(p<=heads[i].end)
                {
                    rpos.pos=heads[i].length-p+heads[i].start-l+2;
                    rpos.name=heads[i].name;
                }
                else
                {
                    cout<<"read wrong, because it matches to 'end marker'"<<endl;
                    rpos.pos=p;
                    rpos.name="WrongRead";
                }
                break;
            }
        }
        return rpos;
    }

private:


    vector<FastaHead> heads;
    RealPosition rpos;
    bool READY;

};




#endif // UTIL_H
