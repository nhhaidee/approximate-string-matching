#include "utils.h"
#include "DataTypes.h"

void DNA_Translate(string *orig_str)
{
    for(string::iterator it=orig_str->begin();it<orig_str->end();it++)
    { 
        
        if(*it=='N' || *it=='n')
        {
            *it='Z'; // Not use N, so change to character N to Z after T
        }     
        if(*it=='a')
        {
            *it='A';
        }
        if(*it=='c')
        {
            *it='C';
        }
        if(*it=='g')
        {
            *it='G';
        }
        if(*it=='t')
        {
            *it='T';
        }
        if(*it=='W' || *it=='w')
        {
            *it='T';
        }
        if(*it=='S' || *it=='s')
        {
            *it='G';
        }
        if(*it=='M' || *it=='m')
        {
            *it='C';
        }
        if(*it=='K' || *it=='k')
        {
            *it='T';
        }
        if(*it=='R' || *it=='r')
        {
            *it='G';
        }
        if(*it=='Y' || *it=='y')
        {
            *it='C';
        }
        if(*it=='B' || *it=='b')
        {
            *it='G';
        }
        if(*it=='D' || *it=='d')
        {
            *it='T';
        }
        if(*it=='H' || *it=='h')
        {
            *it='C';
        }
        if(*it=='V' || *it=='v')
        {
            *it='G';
        }
    }
}


u_int8_t encode(char &c)
{
    u_int8_t i;
    switch(c)
    {
        case 'a':
        case 'A': i=0;break;
        case 'c':
        case 'C': i=1;break;
        case 'g':
        case 'G': i=2;break;
        case 't':
        case 'T': i=3;break;
        case 'z':
        case 'Z': i=4;break; // CHILDREN_SIZE = 4
        default: i=CHILDREN_SIZE+2;break; // i = 6 which is %, CHILDREN_SIZE + 2 = 6
    }

    if(i==CHILDREN_SIZE+2)
    {
        if(c=='$')
            i=CHILDREN_SIZE+1; // i = 4 + 1 = 5,CHILDREN_SIZE + 1 = 5
    }
    return (u_int8_t)i;
}

char decode(size_t i)
{
    char c;
    switch(i)
    {
        case 0: c='A';break;
        case 1: c='C';break;
        case 2: c='G';break;
        case 3: c='T';break;
        case 4: c='Z';break;
        case 5: c='$';break;
        default: c='%';break;
    }
    return c;
}
