
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sys/time.h>
#include <string>
#include <zlib.h>
#include "kseq.h"
#include "SuffixTree.h"
#include "DataTypes.h"

KSEQ_INIT(gzFile, gzread)

using namespace std;

int main()
{

    struct timeval start,finish;
    int l;
    double duration;

    string fasta_file="Cyanidioschyzon_merolae.ASM9120v1.dna.chromosome.1.fa";
    string fastq_file="Cyanidioschyzon_merolae_5000_reads_100bps.fq";
    string fasta_seq; 
    
    gzFile fp_fa;
    kseq_t *seq;
    fp_fa = gzopen(fasta_file.c_str(), "r");
    seq = kseq_init(fp_fa);   
    while ((l = kseq_read(seq)) >= 0)
    {
        //FastaHead head(seq->name.s,seq->seq.l);
        fasta_seq.append(seq->seq.s);
        //fasta_seq.push_back('%'); // Test with Ecoli first
    }
    //fasta_seq_orig = fasta_seq;
    fasta_seq.push_back('$');
    DNA_Translate(&fasta_seq);
    cout << "Genome Length: " << fasta_seq.length() << endl;
    kseq_destroy(seq);
    gzclose(fp_fa);
    for (unsigned int i = 0; i< fasta_seq.length(); i++)
    {
        if (fasta_seq[i]=='%')
          cout <<  fasta_seq[i] << endl;
    }

    SuffixTree stree;
    stree.buildSuffixTree(fasta_seq);
    string pattern = "CGCGTGCAACTCTGAGTCGTGTGGTTAGACGTTGGACCAGCAAGTCCGTTCCAGCAAGCCAAGGCGAGGTCTACGCAAAGGAAGACGTGCTTCGCGCTTT";
    int k;
	//getline(cin, pattern);
	cout << "\nInput k errors = ";
	cin >> k;
    cout << "Pattern: " << pattern << endl;
    ::gettimeofday(&start,NULL);
    stree.AMSOverSuffixTree(fasta_seq,pattern,k);
    ::gettimeofday(&finish,NULL);
    duration=finish.tv_sec-start.tv_sec+(finish.tv_usec-start.tv_usec)/1000000.0;
    cout<<"Running Time: "<<duration<<" s"<<endl;
    return 0;
}
