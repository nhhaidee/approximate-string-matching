#include"SuffixTree.h"
#include<string>
#include <stack>
#include<iostream>
#include<algorithm>
#include <stdio.h>
#include "DataTypes.h"

using namespace std;

SuffixTree::SuffixTree()
{
    SuffixTrieHash.rehash(17961079);
    //cout  << "Call Constructor" << endl;
}

SuffixTree::~SuffixTree()
{
    //cout  << "Call Deconstructor" << endl;
}

int SuffixTree::edgeLength(Node *n) {
    if (n == root)
        return 0;
    else
        return *(n->end) - (n->start) + 1;
}

int SuffixTree::walkDown(Node *currNode)
{
    if (activeLength >= edgeLength(currNode))
    {
        activeEdge += edgeLength(currNode);
        activeLength -= edgeLength(currNode);
        activeNode = currNode;
        return 1;
    }
    return 0;
}

void SuffixTree::extendSuffixTree(int pos, string& Text)
{

    leafEnd = pos;
    remainingSuffixCount++;
 
    lastNewNode = NULL;

    while(remainingSuffixCount > 0) 
    {
        if (activeLength == 0)
            activeEdge = pos; //APCFALZ
        if (activeNode->children[encode(Text[activeEdge])] == NULL)
        {          
            activeNode->children[encode(Text[activeEdge])] = newNode(pos, &leafEnd);            
            if (lastNewNode != NULL)
            {
                
                lastNewNode->suffixLink = activeNode;
                lastNewNode = NULL;
            }
        }
        else
        {       
            Node *next = activeNode->children[encode(Text[activeEdge])];
            if (walkDown(next))//Do walkdown
            {
                continue;
            }
            if (Text[next->start + activeLength] == Text[pos])
            {
                if(lastNewNode != NULL && activeNode != root)
                {
                    lastNewNode->suffixLink = activeNode;
                     
                    lastNewNode = NULL;
                }
                //APCFER3
                activeLength++;
                break;
            }
 
            splitEnd = new int;
            *splitEnd = next->start + activeLength - 1;
 
            //New internal node
            Node *split = newNode(next->start, splitEnd);
            activeNode->children[encode(Text[activeEdge])] = split;
 
            //New leaf coming out of new internal node
            split->children[encode(Text[pos])] = newNode(pos, &leafEnd);
            next->start += activeLength;
            split->children[encode(Text[next->start])] = next;

            if (lastNewNode != NULL)
            {
                lastNewNode->suffixLink = split;
            }
 
            lastNewNode = split;
        }
        
        remainingSuffixCount--;
        if (activeNode == root && activeLength > 0) //APCFER2C1
        {
            activeLength--;
            activeEdge = pos - remainingSuffixCount + 1;
        }
        else if (activeNode != root) //APCFER2C2
        {
            activeNode = activeNode->suffixLink;
        }
    }
}

void SuffixTree::buildSuffixTree(string& T)
{ 
    
    //Initialize for building suffix tree
    root = NULL;
    rootEnd = new int;
    *rootEnd = - 1;
    root = newNode(-1, rootEnd); 
    root->substr_len = 0;
    activeNode = NULL;
    activeEdge = -1;
    activeLength = 0;
    remainingSuffixCount = 0;
    leafEnd = -1;
    splitEnd = NULL;
    activeNode = root; //First activeNode will be root
    cout << "Building suffix tree for Text T" << endl;
    for (unsigned int i=0; i< T.length() ; i++)
    {
        extendSuffixTree(i, T);
    }
    int labelHeight = 0;
    depth();// set depth from root
    setSuffixIndexByDFS(root, labelHeight, T);
    root->suffixLink = NULL;
    //testHash();
    //testhash(root);

}


void SuffixTree::depth() //depth from root
{
    stack <Node*> stack;
    //suffixTreeNode[root->id]= root;
    stack.push(root);
    //int counter = 0;
    while(!stack.empty())
    {
        Node* parent = stack.top();
        if (parent == root)
        {
            root->Parent =NULL;
        }
        stack.pop();  
        for (int i = 0; i < MAX_CHILD; i++)
        {
            if (parent->children[i] != NULL)
            {
                parent->children[i]->substr_len = *parent->children[i]->end - parent->children[i]->start + 1 + parent->substr_len;
                parent->children[i]->Parent = parent;
                stack.push(parent->children[i]);
            }
        }
    }
}

void SuffixTree::setSuffixIndexByDFS(Node *n, int labelHeight, string& T)
{
    if (n == NULL)  
        return;
    int leaf = 1;
    for (int i = 0; i < MAX_CHILD; i++)
    {
        if (n->children[i] != NULL)
        {
            //total_nodes+=1;
            
            if (leaf == 1 && n->start != -1)
            {
               //cout <<  "[" << n->suffixIndex << "]"  << ", [" << n->start << ", " << *n->end << "]" << " Node: " << n << " Parent: " << n->Parent << " suffixLink: " << n->suffixLink <<  " Edge Length: " << n->substr_len   <<endl; 
            }
            
            leaf = 0;
            setSuffixIndexByDFS(n->children[i], labelHeight + edgeLength(n->children[i]), T);
        }
    }
    if (leaf == 1)
    {
        n->suffixIndex = T.length() - labelHeight;
        //cout << n->suffixIndex << endl;
        //cout <<  "[" << n->suffixIndex << "]"  << ", [" << n->start << ", " << *n->end << "]" << " Node: " << n << " Parent: " << n->Parent << " suffixLink: " << n->suffixLink <<  " Edge Length: " << n->substr_len  << endl;
    }
}

