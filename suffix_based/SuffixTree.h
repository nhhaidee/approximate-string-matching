#ifndef SuffixTree_H
#define SuffixTree_H

#include<iostream>
#include<string>
#include<vector>
#include <stdlib.h> 
#include <array>
#include <unordered_map>
#include<iterator>
#include <algorithm>
#include "math.h"
#include "DataTypes.h"
#include <functional>
#include <chrono>
#include <stack>


using namespace std;
using namespace std::chrono;

class SuffixTree
{   
    
    private:
        struct SuffixTreeNode 
        {
            SuffixTreeNode *children[MAX_CHILD];
            SuffixTreeNode *suffixLink;
            SuffixTreeNode *Parent;
            int start;
            int *end;
            int substr_len; // Label length of substring
            int suffixIndex; // Internal node has suffix index is -1
        };
        typedef SuffixTreeNode Node; 
        
        Node *newNode(int start, int *end) // Suffix Tree Node
        {
            Node * node = new Node;
            int i;
            for (i = 0; i < MAX_CHILD; i++)
                node->children[i] = NULL;
            node->suffixLink = root;
            node->start = start;
            node->end = end;
            node->suffixIndex = -1;
            node->substr_len = 0;
            node->Parent = NULL;
            return node;
        }  

        struct SuffixTrieNode // To insert to Hash Table
        {
            Node* treeNode; // This is a node of Suffix Tree
            vector<int>* D;
            vector<int>* L;
        };

        struct State{ // To simulate a Trie Node, use for goto transition
            Node* treeNode; // This is a node of Suffix Tree
            vector<int>* D;
            vector<int>* L;
            int offset; // An Offset down to edge string of a node
            int state_top;
            //int position; // Position of a character which is inserted into Hash Table
        };

        struct hash_node{
            size_t operator()(const Node *n) const
            {
                return (unsigned long) n >> 4;
            }
        };

        Node* root;
        Node* key;
        Node* lastNewNode;
        Node* activeNode;
        //const void *suffixTrieNode = root;
        //cout << *suffixTrieNode << endl;
        int remainingSuffixCount;
        int leafEnd;
        int activeEdge;
        int activeLength;
        int *rootEnd;
        int *splitEnd;

        SuffixTrieNode suffixTrieNode;
        State state; // Use for goto-transition
        int nodeLength;
        vector<int>* D_visited;
        vector<int>* L_visited;
        string Fasta;
        int total_go_down =0;
        int new_top = 0;
        int node_top;
        unordered_multimap<Node*,State,hash_node>SuffixTrieHash;
        typedef unordered_multimap<Node*,State,hash_node>::iterator rangeIt;
        pair<rangeIt, rangeIt>range;
        
    public:

        SuffixTree();
        ~SuffixTree();
        //////////////////////////Functions for suffix tree of pattern////////////////////////////
        int edgeLength(Node *n);
        int walkDown(Node *currNode);
        void extendSuffixTree(int pos, string& pattern);
        void setSuffixIndexByDFS(Node *n, int labelHeight, string& T);
        void depth();
        void freeSuffixTree(Node *n);
        void buildSuffixTree(string& T);

        State goto_transition(State &r, char &c)
        {
            //cout << r.treeNode << " " << r.offset+r.treeNode->start+1 << " " <<  *r.treeNode->end << endl;
            if (r.treeNode == root || r.offset+r.treeNode->start+1 > *r.treeNode->end) 
            {
                state.treeNode = r.treeNode->children[encode(c)]; // Jump children of a node
                state.offset = 0; // reset Offset to 0
                state.D = NULL;
                state.L = NULL;
                state.state_top = 0;
            }
            else //if (r.offset+r.trieNode->start +1 <= *r.trieNode->end && ),go down the edge string of a node
            {
                state.treeNode = r.treeNode;
                state.offset = r.offset+1;
                state.D = NULL;
                state.L = NULL;
                state.state_top = 0;
            }
            return state;
        }

        State travel_Edge (Node *n,string &T, char &c, int pos, int node_start) // pos = offset to reach
        {
            nodeLength = *n->children[encode(c)]->end - n->children[encode(c)]->start + 1;
            //cout << "\n Node Travel Edge: " << n->children[encode(c)]<< ", Char travel edge " << c << " " <<  n->children[encode(c)]->substr_len+node_start << endl;
            if (nodeLength < pos +1) // Continue going down
            {
                total_go_down+=1;
                travel_Edge(n->children[encode(c)],T,T[nodeLength+node_start],pos-nodeLength,nodeLength+node_start); 
            }
            else
            {
                state.treeNode = n->children[encode(c)];
                state.offset   = pos;
                state.D        = NULL;
                state.L        = NULL;
                state.state_top = 0;
            }
            return state;
        }
     
        State suffix_transition(State &r, string &T)
        {
            if (r.treeNode->Parent->suffixLink == NULL)
            {
                if (*r.treeNode->end == r.treeNode->start) //Edge string has only one character and Parent is root
                {
                    state.treeNode = root;
                    state.offset   = root->start;
                    state.D        = NULL;
                    state.L        = NULL;
                    state.state_top = 0;
                }
                else
                {
                    state = travel_Edge(root,T,T[r.treeNode->start+1],r.offset-1,r.treeNode->start+1);
                }      
            }
            else //if (r.treeNode->Parent->suffixLink != root) // Follow SuffixLink and do gown if needed
            {
                if (*r.treeNode->Parent->suffixLink->children[encode(T[r.treeNode->start])]->end \
                    -r.treeNode->Parent->suffixLink->children[encode(T[r.treeNode->start])]->start < r.offset)
                {
                    state = travel_Edge(r.treeNode->Parent->suffixLink,T,T[r.treeNode->start],r.offset,r.treeNode->start);
                }
                else
                {
                    state.treeNode = r.treeNode->Parent->suffixLink->children[encode(T[r.treeNode->start])];
                    state.offset   = r.offset;
                    state.D        = NULL;
                    state.L        = NULL;
                    state.state_top = 0;
                }
            }
            return state;
        }
        
        void dynamic_programming(vector<int>* D1, vector<int>* L1, vector<int>* D2, vector<int>* L2, string& q, char N)
        {
            int cost,j;
            D2->push_back(0);
            L2->push_back(0);
            for (j = 1; j <= q.length(); j++){
                D2->push_back(D2->at(j-1) + 1);
                L2->push_back(L2->at(j-1));
                if (q[j-1] == N)
                    cost = 0;
                else
                    cost = 1;
                if (D1->at(j - 1) + cost < D2->at(j)){
                    if (cost == 0){
                        D2->at(j) = D1->at(j-1);
                    }                       
                    else{
                        D2->at(j) = D1->at(j-1) + 1;
                    }   
                    L2->at(j) = L1->at(j - 1) + 1;
                }
                if (D1->at(j) + 1 < D2->at(j)){
                    D2->at(j) = D1->at(j) + 1;
                    L2->at(j) = L1->at(j) + 1;
                }
            }
        } // dp function

        int e_dynamic_programming(vector<int>* D1, vector<int>* L1, vector<int>* D2, vector<int>* L2, \
                                  string& q, char& c, int top, int& k)
        {
            int cost,j;
            D2->push_back(0);
            L2->push_back(0);
            if (D1->size() == top)
            {
                D1->push_back(top);
            }
            for (j = 1; j <= top; j++){
                D2->push_back(D2->at(j-1) + 1);
                L2->push_back(L2->at(j-1));
                if (q[j-1] == c)
                    cost = 0;
                else
                    cost = 1;
                if (D1->at(j - 1) + cost < D2->at(j)){
                    if (cost == 0){
                        D2->at(j) = D1->at(j-1);
                    }                       
                    else{
                        D2->at(j) = D1->at(j-1) + 1;
                    }   
                    L2->at(j) = L1->at(j - 1) + 1;
                }
                if (D1->at(j) + 1 < D2->at(j)){
                    D2->at(j) = D1->at(j) + 1;
                    L2->at(j) = L1->at(j) + 1;
                }
            }
            new_top = top;
            while(D2->at(new_top) > k)
            {
                new_top = new_top - 1;
            }
            if (new_top < q.length())
            {
                new_top = new_top + 1;
            }
            return new_top;
        } // dp function

        bool checkVisited(State& s)
        {
                range = SuffixTrieHash.equal_range(s.treeNode);
                for (; range.first != range.second; ++range.first)
                {
                    if (s.offset == range.first->second.offset)
                    {
                        D_visited = range.first->second.D;
                        L_visited = range.first->second.L;
                        node_top  = range.first->second.state_top;
                        return true;
                    }    
                }

            return false;
        }
        int state_depth(State& r)
        {
            if (r.treeNode == root)
                return 0;
            else
                return (r.treeNode->substr_len - (*r.treeNode->end- r.treeNode->start+1) + r.offset + 1);
        }

        int max_index(vector<int> *D,int k)
        {
            int max = 0;
            for (int i = D->size()-1; i >=0; i--)
            {
                if (D->at(i) <=k)
                {
                    max = i;
                    break;
                }
            }
            return max;
        }        

        void AMSOverSuffixTree(string& T, string& Pattern,int k)
        {
            int m = Pattern.length();
            int n = T.length();
            int i,j,q = 0,Q = 0,top = 0;
            char c;
            State s,r;
            vector<int> *D_0;
            vector<int> *L_0;
            vector<int> *D_tmp;
            vector<int> *L_tmp;
            vector<int> *D1;
            vector<int> *L1;

            r.treeNode = root;
            r.offset = root->start;
            r.D = NULL;
            r.L = NULL;
            D_0 = new vector<int>;
            L_0 = new vector<int>;
            D_tmp = new vector<int>;
            L_tmp = new vector<int>;
            int total_column_generated = 0;
            int total_state_visited = 0;
            for (i = 0; i <= m; i++)
            {
                D_0->push_back(i);
                L_0->push_back(0);
            }

            //cout << "Total of Buckets before running: " << SuffixTrieHash.bucket_count() << endl;
            for (i = 0; i < n-1; i++)
            {
                c = T[i];
                if (r.treeNode == root)
                {
                    D_tmp = D_0;
                    L_tmp = L_0;
                    top = k+1;
                }
                else
                {
                    D_tmp = r.D;
                    L_tmp = r.L;
                    top = r.state_top;
                }
                s=goto_transition(r,c);
                if (checkVisited(s))
                {
                    r = s;
                    r.D = D_visited;
                    r.L = L_visited;
                    r.state_top = node_top;
                    total_state_visited++;
                    while(r.D == NULL && r.L == NULL)
                    {
                        r = suffix_transition(r,T);
                        checkVisited(r);
                        r.D = D_visited;
                        r.L = L_visited;
                        r.state_top = node_top;
                    }
                    //Output result
                    if (r.D->size()-1==m)
                    {
                        if (r.D->at(m) <= k)
                            {
                                cout << "Matching Position: " << i << ", Q: " << r.L->at(m) << ", Viable Prefix: " << T.substr((((int)i-int(r.L->at(m)))+1),r.L->at(m)) << ", k : " << r.D->at(m) << "(Visisted)"<< endl;
                            }  
                    }
                }
                else
                {
                    D1 = new vector<int>;
                    L1 = new vector<int>;
                    top = e_dynamic_programming(D_tmp,L_tmp,D1,L1,Pattern,c,top,k);
                    total_column_generated++;
                    r = s;
                    q = max_index(D1,k);
                    Q = L1->at(q);
                    while(state_depth(r) != Q)
                    { 
                        if (checkVisited(r)==false)
                            SuffixTrieHash.emplace(r.treeNode,r);
                        r = suffix_transition(r, T);
                    }
                    r.D = D1;
                    r.L = L1;
                    r.state_top = top;
                    if (checkVisited(r)==false)
                        SuffixTrieHash.emplace(r.treeNode,r);
                    //Output result
                    if (q==m)
                    {
                        cout << "Matching Position: " << i << ", Q: " << Q << ", Viable Prefix: " << T.substr((((int)i-int(Q))+1),Q) << ", k : " << D1->at(r.state_top)<< endl;
                    } 
                }
            }
            /*
            cout << "Total Bucket after running: " << SuffixTrieHash.bucket_count() << endl;
            cout << "Total Column Generated: " << total_column_generated << endl;
            cout << "Total State Visited: " << total_state_visited << endl;
            int total_state = 0;
            for (j=0; j < SuffixTrieHash.bucket_count(); j++)
            {
                if (SuffixTrieHash.bucket_size(j)>0)
                {
                    total_state+=SuffixTrieHash.bucket_size(j);
                    
                    cout << "Bucket #" << j << " has " << SuffixTrieHash.bucket_size(j) << " elements:" << endl;
                    for (auto it = SuffixTrieHash.begin(j); it!=SuffixTrieHash.end(j); ++it)
                    {
                        if (it->second.D != NULL && it->second.L != NULL)
                            std::cout << "Key: " << it->first << "; Node: " << it->second.treeNode << ", Offset: " << it->second.offset << ", D: " << it->second.D->size() << ", L: " << it->second.L->size() << endl;
                        else
                            std::cout << "Key: " << it->first << "; Node: " << it->second.treeNode << ", Offset: " << it->second.offset << ", D: " << it->second.D << ", L: " << it->second.L << endl;                   
                    }
                    cout << endl;
                    
                }
            }
            
            cout << "Total States in Hash Table: " << total_state << endl;
            */
        }

        /////////////////////////////////////////////////////////////////////////////////////////
        // Util functions
        u_int8_t encode(char &c)
        {
            u_int8_t i;
            switch(c)
            {
                case 'a':
                case 'A': i=0;break;
                case 'c':
                case 'C': i=1;break;
                case 'g':
                case 'G': i=2;break;
                case 't':
                case 'T': i=3;break;
                case '$': i=4;break;
                default:  i=5;break; // $
            }
            return (u_int8_t)i;
        }

};


#endif
